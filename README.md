# CV_Game
The development of this game is just in progress. It's one of my independent courseworks at the [HTW Berlin](http://www.htw-berlin.de/).
The game is based on the project [Color Tower Defense](http://www.pygame.org/project-Color+Tower+Defense-1688-3754.html) by [Mad Cloud Games](http://madcloudgames.blogspot.de/).

## Requirements
* [python 2.7.3](http://www.python.org/download/releases/2.7.3/)
* [numpy](http://www.numpy.org/), version needs to be compatible to python 2.7
* [OpenCV](http://opencv.org/), version needs to be compatible to python 2.7
* [pygame 1.9.1](http://www.pygame.org/download.shtml), version needs to be compatible to python 2.7

## Run
To run the debug program call `python cv_game`.
To run the game call `python Color Tower Defense`.

## Profile
`python -m cProfile -s time cv_game.py > profile_simple.out`
`python -m cProfile -o profile.out cv_game.py`

