\chapter{Planung}
Die folgende Planung erläutert Definitionen, Vorbereitungen und Entscheidungen die zur Realisierung des Spiels führen.

\section{Tower Erstellung}
Grundlegend ist die Entscheidung, wie die Tower erstellt werden. Folgende Tabelle zeigt Vor- und Nachteile der angedachten Arten (siehe Kapitel \ref{analyse:spielidee}) auf. Eingeklammerte Stichpunkte sind nur unter bestimmten Umständen zutreffend.

\begin{table}[H]
\begin{tabular}{m{2.3cm}|m{5.4cm}|m{5.4cm}}
\hline  & Pro & Contra \\ 
\hline Zeichnen 
&
\begin{itemize}[leftmargin=*] 
\item Nur Papier und Stift notwendig
\item Einfacher Vorgang
\item Erweiterung möglich
\end{itemize}
&  
\begin{itemize}[leftmargin=*] 
\item (Hoher Papierverbrauch)
\item (Spielbetrug durch Wiederverwendung)
\end{itemize}
\\
\hline Ausschneiden
&  
\begin{itemize}[leftmargin=*] 
\item Nur Papier und Schere notwendig
\item Erweiterung möglich (z.B. anmalen)
\end{itemize}
&
\begin{itemize}[leftmargin=*] 
\item Relativ schwerer Vorgang
\item Hoher Papierverbrauch
\end{itemize}
\\ 
\hline Lego 
&
\begin{itemize}[leftmargin=*] 
\item Originelle Erstellung
\item Keine Verschwendung von Material
\end{itemize}
&
\begin{itemize}[leftmargin=*] 
\item Lego Steine notwendig
\item Dreidimensionales Objekt lässt sich schwerer zweidimensional detektieren. Noppen und Schatten bedeuten Schwierigkeiten.
\end{itemize}
\\ 
\hline 
\end{tabular}
\caption{Vor- und Nachteile der Tower Erstellungsarten}
\end{table} 
Um die Einstiegshürde in das Spiel möglichst gering zu halten wird das Zeichnen zur Erstellung der Tower eingesetzt. 

\section{Technik}
Für die Umsetzung des Spiels kann auf vorhandene Software zurückgegriffen werden. Dadurch kann ein größerer Funktionsumfang mit geringem Zeitaufwand erreicht werden.

\subsection{OpenCV}
OpenCV (Open Source Computer Vision Library)\footnote{\url{http://opencv.org/}} ist eine Softwarebibliothek für CV und maschinelles Lernen. Interfaces für C++, C, Python und Java sind vorhanden und unterstützen die gängigen Plattformen. Veröffentlicht wird OpenCV unter der BSD-Lizenz. \footnote{Software unter BSD-Lizenz darf frei verwendet werden, siehe \url{http://opensource.org/licenses/bsd-license.php}.}\footnote{\Vgl\Zitat{opencvabout}} \\
Der große Funktionsumfang und die ausführliche Dokumentation sind wichtige Gründe für den Einsatz von OpenCV in dieser Arbeit.\footnote{OpenCV API Reference \url{http://docs.opencv.org/2.4.6/modules/refman.html}}\\
Die aktuelle Version von OpenCV ist 2.4.6.\footnote{Stand 20.09.2013, siehe \url{http://opencv.org/downloads.html}.}

\subsection{Programmiersprache}
Als Programmiersprache kommen nur die von OpenCV unterstützten Sprachen infrage. Aufgrund der Einfachheit, den zahlreichen weiteren Bibliotheken und der Möglichkeit OpenCV einfach nativ (C++) zu nutzen wird die Programmiersprache Python verwendet.\\
Derzeit OpenCV unterstützt nur Python 2.x, sodass die aktuelle Version Python 2.7.5 genutzt wird.\footnote{Stand 20.09.2013, siehe \url{http://www.python.org/download/}.} 

\subsection{Pygame}
Pygame\footnote{\url{http://www.pygame.org/}} ist eine Sammlung vom Python Modulen zur Spieleprogrammierung. Viele mit Pygame entwickelte Spiele sind frei verfügbar, sodass ein Basisspiel verwendet werden kann (siehe \ref{Basisspiel}).

\subsection{numpy}
``NumPy is the fundamental package for scientific computing with Python.''\footnote{\Vgl\Zitat{numpy}} In OpenCV Funktionen können NumPy Objekte verwendet werden. Ansonsten eignet sich NumPy auch ausgezeichnet zur Vereinfachung, Beschleunigung und Absicherung von Funktionen.

\subsection{Webcam}
Die verwendete Webcam muss für die Tower Detektion und Berechnung der Features eine ausreichende Auflösung besitzen. Eine Auflösung von 640x480 Pixeln ist die Minimalanforderung. Hierbei muss der Spieler die erstellten Tower nah an die Webcam heran bringen. Bei einer höheren Auflösung kann der Abstand zwischen Kamera und Tower höher sein. Zu beachten ist, dass eine höhere Auflösung eine rechenintensivere Detektion zur Folge hat.
\\ 
Für die bessere Handhabung wird empfohlen die Webcam auf einem Stativ zu befestigen. Die Webcam sollte dabei so ausgerichtet sein, dass der Spieler die Tower bequem erstellen kann.

\pagebreak
\section{Features}
\label{planung:features}
Ein Feature kann als ``einer Sache eigenes Merkmal''\footnote{\Vgl\Zitat{duden:feature}} definiert werden. Das heißt, ein Feature beschreibt in der CV bestimmte Charakteristika von Bildern oder Objekten in Bildern.
\\
Wie bereits in Kapitel \ref{Analyse:Features}  beschrieben, können verschiedene Features zur Erkennung von erstellten Towern genutzt werden, unabhängig davon auf welche Art sie erstellt wurden.

\begin{figure}[H]
\label{pic:featureablauf}
\centering
\includegraphics[width=1\textwidth]{./Bilder/feature_ablauf.png}
\caption{Ablauf der Feature Bestimmung}
\end{figure}


Zur Berechnung von Features der Tower ist es zwangsläufig notwendig, dass zuerst das Bild aus der Webcam verarbeitet wird und eine Tower Detektion erfolgt. Dies bedeutet, dass das Bild nach möglichen Objekten durchsucht wird, welche ein Tower sein könnten. Ein Tower muss dabei ein geschlossenes Objekt, nachfolgend auch \textit{Shape} genannt, sein, das sich erkennbar von dem Hintergrund abhebt.\\
Die folgende Abbildung zeigt alle gefundenen Shapes (rechts, grau) in dem Eingabebild (links) an. Shapes die später als valide Tower erkannt werden sind grün markiert. 

\begin{figure}[H]
\label{pic:allpossibleshapes}
\centering
\includegraphics[width=1\textwidth]{./Bilder/all_found_shapes.png}
\caption[Alle gefundenen Shapes]{Alle gefundenen Shapes (grau). Spätere Tower grün markiert.}
\end{figure}

Die möglichen groben Features aus Kapitel \ref{Analyse:Features} werden nun genauer analysiert und bewertet.

\subsection{Umfang}
Der Umfang, ``die Länge der eine Fläche begrenzenden Linie'' \footnote{\Vgl\Zitat{duden:umfang}}, eines Shapes ist einfach zu berechnen und kann für Größenrelationen genutzt werden. Eine Approximation ist möglich. Es ist zu beachten, dass die Shapes verschiedene Abstände zur Webcam haben können und somit absolute Vergleiche nicht möglich sind.
\subsection{Drehung}
Die Berechnung der Drehung eines Shapes ist komplexer und bedarf Vorarbeit, da die Drehung ausgehend von definierten Punkten auf bzw. in dem Shape berechnet werden. Ein Einsatz ist vorstellbar, jedoch nicht zu bevorzugen.
\subsection{Größe / Flächeninhalt}
Wie der Umfang ist auch die Größe eines Shapes einfach zu berechnen und kann für relative Vergleiche eingesetzt werden, wobei die möglichen unterschiedlichen Abstände zur Webcam zu beachten sind.
\subsection{Farbe}
Die Farbe des Shapes oder einzelner Regionen in einem Shape ist gut zu ermitteln. Es sind mehrere Typen für das Farbfeature möglich:
\begin{itemize}
\item Durchschnittsfarbe
\item Dominante Farbe
\item Farbhistogramm
\item Farbverteilung
\end{itemize}
Eine Prüfung der Typen ist notwendig. Dabei spielt auch die Belichtung eine große Rolle. Farben werden durch Beleuchtung mitunter stark verändert, was in der Berechnung zu beachten ist.\\
Außerdem kann ein Wechsel des Farbraums von RGB zu HSV oder YCC zur Robustheit des Features beitragen, was ebenso ausgetestet werden muss.\\
Das Farbfeature ist für den Spieler ein einfach zu benutzen, birgt jedoch die Gefahr der Verfälschung durch Beleuchtung. Ist eine robuste Berechnung und ein robuster Vergleich möglich, so ist das Feature empfehlenswert.
\subsection{Form / Kontur}
Diverse kleine Features der Kontur des Shapes können ermittelt werden.
\begin{itemize}
\item Kompaktheit
\item Kantenanzahl
\item Winkel
\item Schachtelung von Formen / Konturen
\item Schwerpunkt
\item Dichte
\end{itemize}
\subsection{relative Position zu anderen Objekten}
Ein Tower kann nicht nur aus einem Shape bestehen, sondern auch aus mehreren die eine gewisse örtliche Relation zueinander haben.

\section{Metriken}
``Metriken beschreiben Rechenvorschrift zur Bestimmung des Unterschieds bzw. des Abstands zwischen Features.'' \footnote{\Vgl\Zitat[S.19]{barthel:ir}}\\
Dabei müssen vier Regeln\footnote{\Vgl\Zitat[S.20]{barthel:ir}} \footnote{\Vgl\Zitat[S.166]{schmitt}} erfüllt werden.
\begin{enumerate}
\item \textbf{Selbstidentität:} $\forall x \in \mathbb{R} : d(x,x) = 0$
\item \textbf{Positivität:} $\forall x \neq y \in \mathbb{R} : d(x,y) > 0$
\item \textbf{Symmetrie:} $\forall x, y \in \mathbb{R} : d(x,y) = d(y,x)$
\item \textbf{Dreiecksungleichung:} $\forall x, y, z \in \mathbb{R} : d(x,z) \leq d(x,y) + d(y,z)$
\end{enumerate}
Für die meisten Features sind die $L_{1}$ und $L_{2}$ Metrik, auch bekannt als Manhattan-Distanz und euklidische Distanz, ausreichend.\footnote{\Vgl\Zitat[S.255]{lmetric:def}}

\section{Abgrenzungen}
Das Spielkonzept und die CV Nutzung können beliebig komplex werden. Aus diesem Grund sind einige Abgrenzungen vorzunehmen.

\subsection{Basisspiel}
\label{Basisspiel}
Das zu entwickelnde Spiel basiert auf einem gegebenen Pygame Spiel. In der öffentlichen Sammlung an Pygame Tower Defense Spielen fallen zwei in die engere Auswahl.
\begin{itemize}
\item {Color Tower Defense\footnote{\url{http://pygame.org/project/1688/}}(CTD)}
\item {PyTowerDefense\footnote{\url{http://www.pygame.org/project-PyTowerDefense-1332-2571.html}}}
\end{itemize}
Die Analyse des Quellcodes ergibt, dass sich \textit{Color Tower Defense} am Besten für eine Erweiterung um CV und eine angepasste Spiellogik eignet.

\subsection{Spielumfang}
Der Spielumfang geht aufgrund des Umfangs dieser Arbeit nicht über den eines Prototypen hinaus. Die Grundfunktionen werden implementiert und eingesetzt, jedoch Zusatzfunktionen eingeschränkt. Der Fokus dieser Arbeit liegt auf dem Aspekt der Computer Vision und nicht dem der vollständigen Spielentwicklung.