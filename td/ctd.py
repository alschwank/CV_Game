############################################################################################
# Color Tower Defense
# Source Code
# programed by Josh Cockrell.
# 
# contact Josh Cockrell @ MadCloudGames@gmail.com with any comments, questions, or changes
#
# -----This game is a work in progress-----
# This project is released under the GNU General Public License V3
# You dont need to ask to use this code but
# we would love to hear what you think about it!
############################################################################################
################################################################################
# madcloudgames.blogspot.com
############################################################################################
############################################################################################

#"what is art?": any non-functional source of value is art. 
# PEOPLE WHO HELPED
# Eli Benderski for his amazing website
# Dr0id for his tile loader module and help with getting map collisions working
# HydroKirby for help with py2exe/python logic
# EmCeeMayor for help with py2exe
# nClam for save file help/os.path.join help/resource packaging
# exarcun for save file format help
# themissinglint for help with display updates/play testing/balancing
# lolrus for linux testing
# huge thanks to Tess and Tyler for artwork
# HUGE thanks to Tyler for music
# Ryan Brown
"""

BEFORE RELEASE CHECKLIST
    make all the save files new
    make player open maps to default
    fix corrupt images
    turn sound on
    set money to default


FEEDBACK:
more emphasis on next wave, pause game, and main menu button.
use game over music for ingame music
arrow on in side of path
build tower mode cancel if you have less than 100 and click
wave life and player life are confusing
income curve is weird.
As soon as I can afford a 2 nature towers, the game gets pretty easy.
But until then I have a really hard time.
make it easier to move towers around
nature/fly/bee is overpowered.
no slow or splash
display next wave info


LIKES:
towers can miss
first ones get 100% back
The variety in towers you have is really sweet.
different from normal tower defense

IN GAME SONG:
strongest part of the song is the part that's just the bassline and drums
And he liked the first synth wash over it.
But to use the synth very sparingly.
the strongest part is the second breakdown.  There is only bass and drumming.. The more simple of the two bass patterns.


os.path.join:
nClam: Yes, to use os.path.join you specify the directories as arguments like ("dir1","dir2","filename.ext")
nClam: You should probably also store the current working directory somewhere so you can do something
like if os.path.exists(os.path.join(cwd,"dir1","dir2","filename.ext")): do some code, else: print error file not found

--------------------------------------------------------------------------------

PRIORITY 1- Urgent!
    balancing (not too hard but still a challenge, no "best" way to play) ALWAYS CURRENT
        different variety of enemies (colors, could help with balancing)

    Fix sell buttons FIXED
    get rid of t (kill all enemies on screen) FIXED
    infinite money glitch FIXED
    fix mac source code files FIXED
    stop wrong maps from being drawn FIXED
    custom icon - use a resource editor FIXED
    give homing II a name FIXED
    fix pointing arrow FIXED
    rename Hydro DEW to DEW and Voltaic arc to Hydro DEW FIXED
    credits FIXED
    make rotation bool FIXED
    package resources inside of the exe/installer (is this possible???)(not the best way[aClam]) CANCELED
    make difficulty select screen FIXED
    switch over to os.path.join FIXED
    better bee towers (more attack) FIXED
    handle all maps completed (game won) FIXED
    load game menu buttons(delete/save file buttons) FIXED
    scaling (making new basic towers cost more after bought/ making enemies exponentially increase difficuilty) FIXED
    handle single map completed (map complete screen) -FIXED
    save game function (auto) FIXED
    load game function displays after main menu FIXED

PRIORITY 2- Needs

    homing V2 are too OP FIXED
    credits include me for art FIXED
    get rid of all sounds except 'Soundeffects','Percs','Chip_Perc_8.wav' FIXED
    galaxy shot more like planet shots FIXED
    know which direction the enemies come from (arrow maybe) FIXED
    tower replacement mode CANCELED
    tech tower paths FIXED
    orbital shots respawn time -FIXED
    fix delete file button FIXED
    change pygame window header text FIXED
    beam targeting type FIXED
    info box displays not enough money/ other errors...  FIXED
    hotkeys FIXED
    fade out and into game FIXED
    fly tower image FIXED
    fix in game menu buttons FIXED
    fix pause screen image FIXED
    towers more spread out in tower paths (redo paths? too little in nature) -FIXED
    shrink tower placement info box FIXED
    orbital shots auto destruct after orbital is destroyed FIXED
    orbital shots maximun number FIXED
    orbital shots range issues FIXED
    raise shot kill boarders (homing disapear FIXED
    get rid of prints FIXED
    hotkeys FIXED
    new homingII image (just more flashy homingI) FIXED
    make slows shoot at front of line CANCELED


PRIORITY 3- Would be nice
    in game sounds CURRENT

    make basic text black FIXED
    infinite game loop depth FIXED
    make map.draw, HUDs.draw go faster (performace speed) FIXED
    new shot colors - FIXED
    rotate towers towards target - FIXED
    toggle sound button (ON/OFF) - FIXED
    center and crop all of the tower images FIXED
    flamethrower tower - FIXED
    change wave font - FIXED
    fix in game buttons to be python generated instead of photoshop images -FIXED
    fix while loop lag in mad_cloud_games startup FIXED
    nicer bee tower image (outline/shading?) -Tyler/Chris FIXED
    machine gun tower shoots faster, weaker, and more inaccurate FIXED
    change question marks in select map menu to locks - CANCELED
    better image rotation http://www.leptonica.com/rotation.html - CANCELED
    cant hit enemies off screen CANCELED





Josh Cockrell - Technical Director, Systems Architect, Design, Gameplay, Audio

Tyler Christensen - Music, Art

Tess Bybee - Art

Chris Breinholt - Critical decision choices concerning white colored robots. (Among other things)

Special Thanks - Dr0id, HydroKirby, EmCeeMayor, nClam, Eli Benderski,
exarcun, themissinglint, lolrus

"""

##### Imports #####
import pickle

import pygame
import numpy as np

from constants import *
from recognizer import TowerRecognizer
from dumbmenu import dumbmenu
from player import Player
import tiledtmxloader
from HUDs import *
from Sprites import Tower_Range, Tower, TowerPlacer, TowerUpgrader, Manual_Shot, Enemy
from reset_game import *


class Menu():
    def __init__(self):
        self.screen = pygame.display.set_mode(WINDOW_SIZE)
        pygame.init()
        pygame.display.set_caption('Color Tower Defense')
        pygame.display.update()
        music.play(music.channel_menu, music.menu_music, True)

    def clear(self):
        self.screen.fill(BLACK)
        pygame.display.update()

    def create_menu(self, points, menu_text=(), x_pos=320):
        return dumbmenu(self.screen, points, menu_title=GAME_TITLE, menu_text=menu_text,
                        x_pos=x_pos, y_pos=250,
                        font="sourcecodepro", size=32, distance=1.0,
                        menu_text_color=LIGHT_GRAY, cursorcolor=WHITE)

    def main(self):
        self.clear()

        selection = self.create_menu(["Start Game", "Credits", "Quit Game"], menu_text=["Welcome!"])
        still_in_game = False

        while True:
            if selection == 0 or still_in_game:
                if not still_in_game:
                    res = self.start_game()

                if res == "quit to menu":
                    self.clear()
                    selection = self.create_menu(["Start Game", "Credits", "Quit Game"], menu_text=["Welcome!"])
                elif res == -1:
                    break
                elif res is None:
                    selection = self.start_game()
                    still_in_game = False
                elif res == "finished game":
                    self.finished_game()
                    break
                elif res == "finished_level":
                    selection = self.finished_level()
                    if selection == 0:
                        self.player.map += 1
                        res = self.run_ctd()
                        still_in_game = True
                    else:
                        break
                elif res == "game over":
                    selection = self.game_over()
                    if selection != 0:
                        break
                    still_in_game = False
                    self.clear()
                    selection = self.create_menu(["Start Game", "Credits", "Quit Game"], menu_text=["Welcome!"])
            elif selection == 1:
                res = self.credits()
                if res == -1:
                    break
                else:
                    self.clear()
                    selection = self.create_menu(["Start Game", "Credits", "Quit Game"], menu_text=["Welcome!"])
            elif selection == -1 or selection == 2:
                break

        pygame.quit()

    def run_ctd(self, recognizer=None):
        self.clear()
        font = pygame.font.SysFont("sourcecodepro", 32)
        text = font.render("Loading...", True, LIGHT_GRAY)
        text_rect = text.get_rect()
        text_rect.center = (WIDTH / 2, 240)
        self.screen.blit(text, text_rect)
        pygame.display.update()
        ctd = CTD(self.player, self.screen, recognizer, self.difficulty)
        return ctd.run()

    def select_difficulty(self):
        self.clear()
        self.difficulty = -1
        while not (0 <= self.difficulty <= 2):
            self.difficulty = self.create_menu(['Easy', 'Normal', 'Hard'], menu_text=["Select the difficulty."])

    def start_game(self):
        self.clear()
        selection = self.create_menu(['A', 'B', 'C', 'Delete Files'], menu_text=["Select a game slot."])

        if 0 <= selection <= 2:
            saved_game = Saved_Game(selection)
            self.player = saved_game.load_game()
            self.select_difficulty()
            recognizer = self.select_map()
            if recognizer is not None:
                return self.run_ctd(recognizer)
        elif selection == 3:
            saved_game = Saved_Game(None)
            saved_game.delete_all_files()
        return -1

    def calibrate(self):
        self.clear()
        recognizer = TowerRecognizer(self.difficulty)
        res = recognizer.calibrate_pygame(self.screen, self.player)

        if res == 0:
            return recognizer
        else:
            return None

    def select_map(self):
        self.clear()
        open_maps = self.player.open_maps

        menu_items = []
        for i, is_open in enumerate(open_maps):
            if is_open:
                menu_items.append('Level ' + str(i + 1))

        self.player.map = self.create_menu(menu_items, menu_text=["Select a map."])
        if self.player.map != -1:
            return self.calibrate()
        return None

    def finished_game(self):
        self.clear()
        self.create_menu(['Quit.'], menu_text=["Congratulations! You won the game!"])
        pygame.quit()

    def finished_level(self):
        self.clear()
        return self.create_menu(['Next Level', 'Quit'], menu_text=["Great. You finished the level. Get on with it!"])

    def pause(self):
        self.create_menu(['Continue'], menu_text=["Pause."])

    def game_over(self):
        self.clear()
        self.screen.fill(RED_RGB)
        return self.create_menu(['Restart.', 'Quit'], menu_text=["The enemy won. Don't let it get you down."])

    def credits(self):
        self.clear()
        return self.create_menu(['Continue'],
                                menu_text=["This game was developed by:", "Alexander Schwank (alschwank@gmail.com)", "",
                                           "Original Color Tower Defense by:",
                                           "Josh Cockrell (MadCloudGames@gmail.com)", "",
                                           "",
                                           "This is a open source project available on github",
                                           "(https://github.com/AlexSchwank/CV_Game)"],
                                x_pos=50)

class Saved_Game():
    def __init__(self, index):
        self.saved_game_number = index

    def delete_all_files(self):  # creates a new save file ontop of the previous one
        for i in range(0, 3):
            player_data = Player()
            self.create_file(i, player_data)

    def load_game(self):
        if not os.path.isfile(((os.path.join('Resources', 'Save Data', 'savedfile') + str(self.saved_game_number)))):
            player_data = Player()
            self.create_file(self.saved_game_number, player_data)
        with open((os.path.join('Resources', 'Save Data', 'savedfile') + str(self.saved_game_number)),
                  'r') as save_file:
            player = pickle.load(save_file)
        return player

    def create_file(self, file_number, player_data):
        with open((os.path.join('Resources', 'Save Data', 'savedfile') + str(file_number)), 'w') as save_file:
            pickle.dump(player_data, save_file)


class CTD():
    def __init__(self, player, screen, recognizer=None, difficulty=0):
        self.player = player
        self.screen = screen
        self.recognizer = recognizer
        self.difficulty = difficulty

        for group in enemies, towers, shots, life_bars:
            for obj in group:
                obj.kill()

    def info_menu(self, menu_type, tower=None, tower_type=None):

        if menu_type == 'tower':
            Button(0, 'tower info', tower, tower_type)

        elif menu_type == 'tower placement':
            Button(0, 'tower info', tower, tower_type)
            Button(1, 'cancel placement', tower, tower_type)

        elif menu_type == 'tower upgrading':
            Button(0, 'upgrade info', tower, tower_type)
            Button(1, 'cancel upgrading', tower, tower_type)

    def clear_info_menu(self):
        for b in buttons:
            b.kill()

    def name_cmp(self, enemy1, enemy2):
        if enemy1.total_distance_traveled > enemy2.total_distance_traveled:
            return -1
        elif enemy1.total_distance_traveled < enemy2.total_distance_traveled:
            return 1
        else:
            return 0

    def sort_enemies_group(self, enemies):
        new_list = []

        for e in enemies:
            current_testing_enemy = 0  # number slot were testing
            found_slot_for_enemy = False
            while not found_slot_for_enemy:
                if len(new_list) > current_testing_enemy:  # if the enemy is even there
                    adjust_slot = self.name_cmp(e, new_list[current_testing_enemy])

                    if adjust_slot == 1:  # if the new enemy has traveled less than already tested enemies
                        current_testing_enemy += 1  # move up in the list to test
                        found_slot_for_enemy = False

                    if adjust_slot == 0:  # if the new enemy and the tested enemy have traveled the same
                        found_slot_for_enemy = True

                    if adjust_slot == -1:  # if the new enemy has traveled more than already tested enemies
                        found_slot_for_enemy = True

                else:  # there are no more enemies in the new list to check with
                    found_slot_for_enemy = True
            new_list.insert(current_testing_enemy, e)

        return new_list

    ###### main game loop #######
    def run(self):

        #Display settings
        pygame.mouse.set_visible(True)  # mouse visible
        clock = pygame.time.Clock()  # opens clock for fps

        music.stop(music.channel_menu)
        music.play(music.channel_game, music.game_music, True)  # play music

        ##### reseting #####
        #clear the groups
        for h in huds:
            h.kill()

        ##### create starting objects #####
        if self.recognizer is None:
            self.recognizer = TowerRecognizer(self.difficulty)
        wave = Wave()
        wave.start_new_map(self.player.map)
        game_map = tiledtmxloader.World_map(
            os.path.join('Resources', 'Map Data', 'map' + str(self.player.map) + '.tmx'))
        self.player.game_map = game_map
        next_wave_timer = 300
        started_next_wave_timer = True

        Background()
        time_to_next_wave_hud = Time_to_next_Wave(next_wave_timer)
        info_popup = Info_PopUp()
        info_bar_background = Info_Bar_Background()
        direction_arrow = Direction_Arrow()
        sound_toggle_button = Sound_Toggle()
        speed_toggle_button = Speed_Toggle()
        pause_button = Pause()
        main_menu_button = Main_Menu()
        next_wave_button = Next_Wave()
        Wave_Info(wave)
        Life(self.player)
        detection_info = Detection_Info()
        manual_shot_button = Manual_Shot_Button()
        shapes_display = Shapes_Display(self.recognizer.shape_collection)
        build_tower_bar = Build_Tower_Bar()

        direction_arrow.start_new_map(self.player)

        ##### general loop values #####
        first_wave_started = False
        placing_tower = False
        upgrading_tower = False
        turret_selected = None
        running = True

        self.player.game_over = False
        self.player.game_won = False
        self.player.quiting = False

        game_paused = False
        active_indices = set()
        manual_shot = None

        ################################################################################
        while running:
            clock.tick(FPS)  # setting frame rate

            game_map.map_collision(pause_button.rect)

            res_img, found_shape_indices, succ_status = self.recognizer.run()
            detection_info.update(succ_status)

            # active buttons to build the detected tower/update
            if found_shape_indices is not None:
                for index in found_shape_indices:
                    active_indices.add(index)
                    build_tower_bar.buttons[index].is_active = True

            ##### Events Mouse / Keyboard #####
            for event in pygame.event.get():

                ##### Quiting #####
                if event.type == pygame.QUIT:  # click the 'x' to quit
                    return "quit to menu"
                elif event.type == pygame.KEYDOWN:

                    ##### escape / space buttons #####
                    if event.key in [pygame.K_ESCAPE, pygame.K_SPACE]:  # hit escape to exit

                        if placing_tower:  # if we're placing a tower
                            placing_tower = False  # not anymore
                            towerplacers.sprite.kill()  # kill the placer
                            for t in tower_range_group:
                                t.kill()  # destroy the tower_range
                            self.clear_info_menu()

                        elif turret_selected:  # if a turret is selected
                            turret_selected = None
                            self.clear_info_menu()  # clear all elements on the info menu
                            for r in tower_range_group:  # clear the tower range
                                r.kill()

                    elif event.key in [pygame.K_n]:
                        if wave.wave_done:  # if the current wave is done

                            wave.start_new_wave()
                            for i in wave_info_group:
                                i.kill()
                            Wave_Info(wave)
                            if not first_wave_started:  # check so we only vanish once
                                first_wave_started = True
                                direction_arrow.vanish()  # go away

                    elif event.key in [pygame.K_p]:
                        game_paused = True  # pause code is at the bottom of the main loop

                ##### Clicking on stuff #####
                elif event.type == pygame.MOUSEBUTTONDOWN:  # all the mouse down events

                    if event.button == 3:  # right mouse button
                        if placing_tower:  # if we're placing a tower
                            placing_tower = False  # not anymore
                            towerplacers.sprite.kill()  # kill the placer
                            for t in tower_range_group:
                                t.kill()  # destroy the tower_range
                            self.clear_info_menu()

                        elif turret_selected:  # if a turret is selected
                            turret_selected = None
                            self.clear_info_menu()  # clear all elements on the info menu
                            for r in tower_range_group:  # clear the tower range
                                r.kill()

                    elif event.button == 1:  # left mouse button
                        object_clicked = False
                        click = pygame.mouse.get_pos()  # gets position of mouse when clicked

                        #####clicking to place a tower #####
                        if placing_tower:
                            canceled = False
                            for b in buttons:  # click on a in game menu button
                                if b.rect.collidepoint(click):  # if we click on a button
                                    if b.button_type == 'cancel placement':
                                        canceled = True
                                        break
                            for button in build_tower_bar.buttons:
                                if button.rect.collidepoint(click):  # if we click on a button
                                    if towerplacers.sprite.tower_type == button.type:
                                        canceled = True
                                        break
                            if canceled:
                                placing_tower = False  # not anymore
                                towerplacers.sprite.kill()  # kill the placer
                                for t in tower_range_group:
                                    t.kill()  # destroy the tower_range
                                self.clear_info_menu()  # clear all elements on the info menu
                            else:
                                for t in towerplacers:
                                    if game_map.is_on_map(click) and not game_map.map_collision(
                                            t.rect):  # if there is no collision with the map
                                        game_map.set_collision(click, 'close')  # block the tile

                                        # build tower on tile
                                        Tower(click, t.tower_type)

                                        placing_tower = False
                                        build_tower_bar.buttons[t.tower_type['id']].is_active = False
                                        self.clear_info_menu()  # clear all elements on the info menu
                                        shapes_display.shape_collection.replace_shape(index=t.tower_type['id'])

                                        # clean up and reset
                                        t.kill()
                                        for tr in tower_range_group:
                                            tr.kill()  # destroy the tower_range
                                    else:
                                        info_popup.display_info('You cannot place a tower there!', True)

                        elif upgrading_tower:
                            canceled = False
                            for b in buttons:  # click on a in game menu button
                                if b.rect.collidepoint(click):  # if we click on a button
                                    if b.button_type == 'cancel upgrading':  # if its an upgrade button
                                        canceled = True
                                        break
                            for button in build_tower_bar.buttons:
                                if button.rect.collidepoint(click):  # if we click on a button
                                    if button.type == UPGRADE:  # if its an upgrade button
                                        canceled = True
                                        break
                            if canceled:
                                upgrading_tower = False  # not anymore
                                tower_upgrader.sprite.kill()  # kill the placer
                                for t in tower_range_group:
                                    t.kill()  # destroy the tower_range
                                self.clear_info_menu()  # clear all elements on the info menu
                            else:
                                for t in towers:
                                    if t.rect.collidepoint(click):  # when a turret is clicked on
                                        if t.upgrade_counter < MAX_UPGRADE_COUNT:
                                            self.clear_info_menu()
                                            t.upgrade()
                                            tower_upgrader.sprite.kill()
                                            build_tower_bar.buttons[UPGRADE['id']].is_active = False
                                            # clean up and reset
                                            for tr in tower_range_group:
                                                tr.kill()  # destroy the tower_range
                                            upgrading_tower = False

                                        else:
                                            info_popup.display_info('Upgrade maximum already reached.', True)
                                    else:
                                        info_popup.display_info('There\'s no tower to upgrade.', True)

                        elif manual_shot_button.is_active:
                            for b in buttons:  # click on a in game menu utton
                                if b.rect.collidepoint(click) or manual_shot_button.rect.collidepoint(click):
                                    if b.button_type == 'cancel shot':  # if its an upgrade button
                                        manual_shot_button.is_active = False  # not anymore
                                        manual_shot_group.sprite.kill()  # kill the placer
                                        for t in tower_range_group:
                                            t.kill()  # destroy the tower_range
                                        self.clear_info_menu()  # clear all elements on the info menu

                            for _ in manual_shot_group:
                                if game_map.is_on_map(click):  # if there is no collision with the map
                                    manual_shot.shot(click, enemies)
                                else:
                                    info_popup.display_info('You cannot shot there!', True)

                        ##### sound on/off button #####
                        elif sound_toggle_button.rect.collidepoint(click):
                            if music.sound_on:
                                for c in music.channels:
                                    c.set_volume(0.0)
                                    music.sound_on = False
                            else:
                                for c in music.channels:
                                    c.set_volume(1.0)
                                    music.sound_on = True
                            sound_toggle_button.change_setting(music.sound_on)
                        #### manual shot button #####
                        elif manual_shot_button.rect.collidepoint(click):
                            manual_shot_button.is_active = True
                            Button(1, 'cancel shot', None, MANUAL_SHOT)
                            manual_shot = Manual_Shot(click)
                        ##### speed up button #####
                        elif speed_toggle_button.rect.collidepoint(click):
                            speed_toggle_button.change_setting()
                            self.player.activated_speed = not self.player.activated_speed
                        ##### pause button #####
                        elif pause_button.rect.collidepoint(click):  # if you click on the pause button
                            game_paused = True  # pause
                        ##### main menu button #####
                        elif main_menu_button.rect.collidepoint(click):  # if you click on the main menu button
                            self.player.quiting = True
                        ##### next wave button #####
                        elif next_wave_button.rect.collidepoint(click):  # if you click on the next wave button
                            if wave.wave_done:
                                started_next_wave_timer = False
                                next_wave_timer = -1
                                time_to_next_wave_hud.show_wave_is_coming()
                                wave.start_new_wave()
                                for i in wave_info_group:
                                    i.kill()
                                Wave_Info(wave)
                                if not first_wave_started:  # check so we only vanish once
                                    first_wave_started = True
                                    direction_arrow.vanish()  # go away

                        ##### check if groups were clicked #####
                        else:
                            ##### build tower buttons #####
                            for button in build_tower_bar.buttons:
                                if button.type == UPGRADE and button.rect.collidepoint(click):
                                    self.clear_info_menu()  # clear all elements on the info menu
                                    for t in tower_range_group:  # clear the tower range
                                        t.kill()
                                    TowerUpgrader(click)
                                    upgrading_tower = True
                                    tower_radius = Upgrade_Stats().radius  # get the radius
                                    Tower_Range(click, tower_radius)  # create a range
                                    self.info_menu('tower upgrading', None, UPGRADE)

                                elif button.rect.collidepoint(click):
                                    self.clear_info_menu()  # clear all elements on the info menu
                                    for t in tower_range_group:  # clear the tower range
                                        t.kill()
                                    TowerPlacer(click, button.type)
                                    placing_tower = True
                                    tower_radius = Tower_Stats(button.type)['radius']  # get the radius
                                    Tower_Range(click, tower_radius)  # create a range
                                    self.info_menu('tower placement', None, button.type)

                            ##### turrets group #####
                            for t in towers:
                                if t.rect.collidepoint(click):  # when a turret is clicked on
                                    object_clicked = True
                                    turret_selected = t
                                    self.clear_info_menu()  # clear all elements on the info menu
                                    for r in tower_range_group:  # clear the tower range
                                        r.kill()
                                    Tower_Range(t.rect.center,
                                                t.range)  # create a tower range at the tower center and with range
                                    self.info_menu('tower', tower=t)  # run the info menu for that tower

                            if not object_clicked:  # if we have not clicked on an object
                                if turret_selected:  # if a turret is selected
                                    turret_selected = None
                                    self.clear_info_menu()  # clear all elements on the info menu
                                    for r in tower_range_group:  # clear the tower range
                                        r.kill()

            ###############################################################################
            # done with user events #

            ##### controls turret: range/targeting/shooting #####
            #put inside a function eventually

            enemies_by_dist_traveled = self.sort_enemies_group(enemies)

            for t in towers:
                # count down till next shot
                if speed_toggle_button.activated_speed:
                    t.shooting_timer -= 2
                else:
                    t.shooting_timer -= 1

                if not t.shoots_at_front:  # if the tower doesnt shoot at the front of the line
                    if len(enemies) > 0:  # if there are enemies on the screen
                        if t.current_target >= len(enemies):  # if we are at the end of the enemies list
                            t.current_target = 0  # reset the target that we are testing

                        t.target = enemies.get_sprite(t.current_target)  # pick which enemy we are testing
                        if not enemies.has(t.target):  # if the target is dead
                            t.current_target += 1  # pick a new target

                        else:  # if the target lives
                            # here we get distance from the tower to the target
                            dist = (math.sqrt((t.target.rect.centerx - t.rect.centerx) ** 2 + (
                                t.target.rect.centery - t.rect.centery) ** 2))
                            if dist > t.range:  # if the target is not in range
                                t.current_target += 1  # test a new target

                            else:  # if the target is in range
                                if t.shooting_timer <= 0:  # if its time to shoot
                                    t.shoot(t.target)  # shoot at the target
                                    t.shooting_timer = t.speed  # reset timer for next shot

                else:  # if the tower shoots at the front of the line
                    if len(enemies_by_dist_traveled) > 0:  # if there are enemies on the screen
                        print t.current_target
                        if t.current_target < 0:
                            t.current_target = (len(enemies_by_dist_traveled) - 1)
                        t.target = enemies_by_dist_traveled[t.current_target]  # pick which enemy we are testing

                        # here we get distance from the tower to the target
                        dist = (math.sqrt((t.target.rect.centerx - t.rect.centerx) ** 2 + (
                            t.target.rect.centery - t.rect.centery) ** 2))
                        if dist > t.range:  # if the target is not in range
                            t.current_target -= 1  # test a new target

                        else:  # if the target is in range
                            if t.shooting_timer <= 0:  # if its time to shoot
                                t.shoot(t.target)  # shoot at the target
                                t.shooting_timer = t.speed  # reset timer for next shot

            ##### control the shots #####
            for s in shots:  # tell every shot which direction to shoot
                s.move(self.player)

            ##### creating a new wave #####
            if wave.update(self.player):  # run the wave update and if it returns true:
                Enemy(self.player, wave)  # create the enemy

            if wave.wave_done and len(enemies) == 0 and not started_next_wave_timer:
                next_wave_timer = TIME_TO_NEXT_WAVE
                started_next_wave_timer = True
                info_popup.display_info('Finished wave.', True)

            if started_next_wave_timer:
                if speed_toggle_button.activated_speed:
                    next_wave_timer -= 2
                else:
                    next_wave_timer -= 1

                if next_wave_timer < 0:
                    wave.start_new_wave()
                    started_next_wave_timer = False
                    for i in wave_info_group:
                        i.kill()
                    Wave_Info(wave)
                    if not first_wave_started:
                        first_wave_started = True
                        direction_arrow.vanish()

            ##### when we run out of life #####
            if self.player.life <= 0:
                self.player.game_over = True

            ##### control game overs #####
            if self.player.game_over:
                info_popup.display_info('GAME OVER', True)
                # the game is now over
                music.channel_game.stop()
                music.play(music.channel_end, music.end_music, True)
                return "game over"

            if self.player.game_won:
                music.channel_game.stop()
                music.play(music.channel_end, music.end_music, True)

                #if we beat the game
                if len(self.player.open_maps) == (self.player.map + 1):
                    return "finished game"
                else:
                    return "finished_level"

            if self.player.quiting:
                return "quit to menu"

            if game_paused:
                music.channel_game.pause()
                music.play(music.channel_end, music.end_music, True)
                menu = Menu()
                menu.pause()
                game_paused = False

            ##### update the sprites #####
            all_update.update(self.player)
            huds.update(self.player)
            shapes_display.update()
            manual_shot_group.update()
            time_to_next_wave_hud.update(next_wave_timer)

            if info_popup.displaying_message:
                info_popup.fade_out()

            ##### draw everything #####
            backgrounds.draw(self.screen)

            map_draw.draw(self.screen)
            all_draw.draw(self.screen)
            enemies.draw(self.screen)
            life_bars.draw(self.screen)
            detection_info_group.draw(self.screen)
            manual_shot_group.draw(self.screen)
            self.screen.blit(info_bar_background.image, info_bar_background.rect.topleft)
            self.screen.blit(time_to_next_wave_hud.image, time_to_next_wave_hud.rect.topleft)

            if placing_tower:  # here because it must be drawn on top of everything
                towerplacers.sprite.move(pygame.mouse.get_pos())  # update the turret placer at the mouse location
                for t in tower_range_group:
                    t.move(pygame.mouse.get_pos())
            elif upgrading_tower:  # here because it must be drawn on top of everything
                tower_upgrader.sprite.move(pygame.mouse.get_pos())  # update the turret placer at the mouse location
                for t in tower_range_group:
                    t.move(pygame.mouse.get_pos())
            elif manual_shot_button.is_active:
                manual_shot.move(pygame.mouse.get_pos())

            huds.draw(self.screen)

            self.screen.blit(shapes_display.image, shapes_display.rect.topleft)
            # rotate numpy array 90 degree and switch colors from BGR to RGB
            self.screen.blit(pygame.surfarray.make_surface(np.rot90(res_img[:, :, ::-1])), (850, 600))

            pygame.display.flip()


if __name__ == "__main__":
    menu = Menu()
    menu.main()