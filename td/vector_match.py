from __future__ import division
import numpy as np
import cv2
from vector import unit_vector

__author__ = 'Alexander'


class Vector2D():
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y
        self.np_array = np.array((x, y))

    def dist_to(self, other):
        #np.sqrt(np.power(self.x - other.x, 2) + np.power(self.y - other.y, 2))
        return np.linalg.norm(self.np_array - other.np_array)


class Line():
    def __init__(self, start=None, end=None, np_array=None):
        if np_array is not None:
            self.start = Vector2D(np_array[0], np_array[1])
            self.end = Vector2D(np_array[2], np_array[3])
        else:
            self.start = start
            self.end = end

        if self.start is not None and self.end is not None:
            self.calc_slope()

    def __str__(self):
        return str(
            "(" + str(self.start.x) + "," + str(self.start.y) + " -> " + str(self.end.x) + "," + str(self.end.y) + ")")


    def calc_slope(self):
        self.slope_v = Vector2D(self.end.x - self.start.x, self.end.y - self.start.y)
        if self.slope_v.x == 0:
            self.slope = float("inf")
            self.b = float("inf")
        else:
            self.slope = self.slope_v.y / self.slope_v.x
            self.b = self.start.y - self.slope * self.start.x
        self.angle = self.angle_x_axis()

    def merge(self, other):
        # merging end from this and end of other.
        self.end = other.end
        self.calc_slope()

    def angle_x_axis(self):
        # based on http://stackoverflow.com/questions/2827393/angles-between-two-n-dimensional-vectors-in-python
        """ Returns the angle in radians between vectors 'v1' and 'v2'::

                >>> angle_between((1, 0, 0), (0, 1, 0))
                1.5707963267948966
                >>> angle_between((1, 0, 0), (1, 0, 0))
                0.0
                >>> angle_between((1, 0, 0), (-1, 0, 0))
                3.141592653589793
        """
        v0 = np.array([self.slope_v.x, self.slope_v.y])
        v1 = np.array([1, 0])
        v0_u = unit_vector(v0)
        v1_u = unit_vector(v1)
        angle = np.arccos(np.dot(v0_u, v1_u))
        if np.isnan(angle):
            if (v0_u == v1_u).all():
                return 0.0
            else:
                return 360.0
        return np.degrees(angle)

    def is_similar(self, other):
        y0 = other.start.x * self.slope + self.b
        y_diff = abs(abs(y0) - abs(other.start.y))
        if y_diff < 7:
            slope_similar = abs(abs(self.angle) - abs(other.angle)) < 5
            if slope_similar:
                return True
            return False
        return False

    def draw(self, img, color):
        cv2.line(img, (self.start.x, self.start.y), (self.end.x, self.end.y), color, 2)

    def length(self):
        return self.start.dist_to(self.end)