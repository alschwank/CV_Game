from __future__ import division
import random
import math
import numpy as np
from constants import MIN_SHAPE_ANGLE, MAX_SHAPE_ANGLE, MIN_EDGE_LENGTH

__author__ = 'Alexander'


class PolygonGenerationAlgorithm():
    def __init__(self):
        self.shape = None
        random.seed()

    def generate_random_points(self, max_size, count):
        points = []
        minXIndex = None
        maxXIndex = None
        for i in range(0, count):
            x = random.randint(0, max_size)
            y = random.randint(0, max_size)

            # check
            if minXIndex is None or x < points[minXIndex][0]:
                minXIndex = i
            if maxXIndex is None or x > points[maxXIndex][0]:
                maxXIndex = i

            points.append(np.array([x, y]))
        return points, minXIndex, maxXIndex


    def generate(self, max_size, point_count):
        pass

    def is_left(self, start_point, end_point, point):
        a = np.array(start_point - point)
        b = np.array(end_point - start_point)
        cross = np.cross(a, b)
        return cross > 0

    def euclid_dist(self, a, b):
        return np.linalg.norm(a - b)

    def perp(self, a):
        # taken from http://stackoverflow.com/questions/3252194/numpy-and-line-intersections
        b = np.empty_like(a)
        b[0] = -a[1]
        b[1] = a[0]
        return b

    def intersects(self, a, b):
        # taken from http://stackoverflow.com/questions/3252194/numpy-and-line-intersections
        da = a[1] - a[0]
        db = b[1] - b[0]
        dp = a[0] - b[0]
        dap = self.perp(da)
        denom = np.dot(dap, db)
        num = np.dot(dap, dp)
        res = (num / denom) * db + b[0]
        return min(a[0][0], a[1][0]) <= res[0] <= max(a[0][0], a[1][0]) \
            and min(a[0][1], a[1][1]) <= res[1] <= max(a[0][1], a[1][1])


    def norm(self, vector):
        # based on http://stackoverflow.com/questions/2827393/angles-between-two-n-dimensional-vectors-in-python
        """ Returns the norm (length) of the vector."""
        # note: this is a very hot function, hence the odd optimization
        # Unoptimized it is: return np.sqrt(np.sum(np.square(vector)))
        return np.sqrt(np.dot(vector, vector))

    def unit_vector(self, vector):
        # based on http://stackoverflow.com/questions/2827393/angles-between-two-n-dimensional-vectors-in-python
        """ Returns the unit vector of the vector.  """
        return vector / self.norm(vector)

    def angle_between(self, a, b):
        # based on http://stackoverflow.com/questions/2827393/angles-between-two-n-dimensional-vectors-in-python
        v0 = a[1] - a[0]
        v1 = b[1] - b[0]
        v0_u = self.unit_vector(v0)
        v1_u = self.unit_vector(v1)
        angle = np.arccos(np.dot(v0_u, v1_u))
        if np.isnan(angle):
            if (v0_u == v1_u).all():
                return 0.0
            else:
                return 360.0
        return np.degrees(angle)


    def is_valid(self, points):
        angles = []
        for i in range(0, len(points) - 1):
            # check angle
            if i == len(points) - 2:
                # special check for last angle, because of equal point at front and end
                angle = abs(self.angle_between([points[i], points[i + 1]], [points[i + 1], points[1]])) % 180
            else:
                angle = abs(self.angle_between([points[i], points[i + 1]],
                                               [points[i + 1], points[(i + 2) % len(points)]])) % 180
            if MIN_SHAPE_ANGLE < angle < MAX_SHAPE_ANGLE:
                # check edge length
                distance = math.sqrt(
                    math.pow(points[i][0] - points[i + 1][0], 2) + math.pow(points[i][1] - points[i + 1][1], 2))
                if distance > MIN_EDGE_LENGTH:
                    # check if there's an intersection
                    line0 = [points[i], points[i + 1]]
                    for k in range(i + 2, len(points) - 1):
                        line1 = [points[k], points[k + 1]]
                        if (line0[0][0] != line1[1][0] or line0[0][1] != line1[1][1]) \
                            and (line0[1][0] != line1[0][0] or line0[1][1] != line1[0][1]):
                            intersects = self.intersects(line0, line1)
                            if intersects:
                                return False, None

                    angles.append(angle)
                else:
                    return False, None
            else:
                return False, None

        return True, angles


class TwoPeasants(PolygonGenerationAlgorithm):
    def generate(self, max_size, point_count):
        result = None
        found = False
        angles = None

        while not found:
            result = []
            points, minXIndex, maxXIndex = self.generate_random_points(max_size, point_count)

            # split point in two groups
            #  - group of points above line (start_point,end_point)
            #  - group of points below line (start_point,end_point)
            start_point = points.pop(minXIndex)
            np.delete(points, start_point)
            if minXIndex < maxXIndex:
                maxXIndex -= 1
            end_point = points.pop(maxXIndex)
            np.delete(points, end_point)

            points_above = []
            points_below = []
            for point in points:
                if self.is_left(start_point, end_point, point):
                    points_below.append(point)
                else:
                    points_above.append(point)

            result.append(start_point)
            current_point = start_point
            while points_above.__len__() > 0:
                nearest_point = points_above.pop(self.get_nearest_point_index(current_point, points_above))
                result.append(nearest_point)
                current_point = nearest_point

            result.append(end_point)
            current_point = end_point
            while points_below.__len__() > 0:
                nearest_point = points_below.pop(self.get_nearest_point_index(current_point, points_below))
                result.append(nearest_point)
                current_point = nearest_point

            result.append(start_point)

            found, angles = self.is_valid(result)

        return result, angles

    def get_nearest_point_index(self, target, points):
        min_dist = None
        index = 0
        for i in range(0, points.__len__()):
            point = points[i]
            dist = self.euclid_dist(target, point)
            if min_dist is None or dist < min_dist:
                min_dist = dist
                index = i

        return index
