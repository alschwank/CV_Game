# coding=utf-8
"""
   Parts of this file are taken from the course https://www.coursera.org/course/compphoto by Irfan Essa and his team.
"""

__author__ = 'Alexander'

import math

from skimage.measure import approximate_polygon
import cv2
from scipy.signal import convolve2d
from scipy.ndimage.filters import gaussian_filter
import numpy as np
from colormath.color_objects import RGBColor

from constants import *


class Image_operations:
    last_contour_count = 0

    def __init__(self):
        self.sobel_x = np.array([[-1, 0, 1],
                                 [-2, 0, 2],
                                 [-1, 0, 1]])

        self.sobel_y = np.array([[-1, -2, -1],
                                 [0, 0, 0],
                                 [1, 2, 1]])
        self.boundaries = np.array([0.375, 0.125, -0.125, -0.375]) * math.pi

    def transform_xy_theta(self, dx, dy):
        dx[dx == 0] = 0.001
        theta = np.arctan(dy / dx)

        return theta


    def transform_xy_mag(self, dx, dy):
        mag = np.sqrt(np.square(dx) + np.square(dy))

        return mag


    def get_color(self, theta, mag):
        # crop the magnitude to 0, 255 range.
        if mag < 0:
            mag = 0

        if mag > 255:
            mag = 255

        # (vertical) | yellow
        if theta > self.boundaries[0] or theta < self.boundaries[3]:
            return 0, mag, mag

        # \ green
        if self.boundaries[3] <= theta < self.boundaries[2]:
            return 0, mag, 0

        # -- blue
        if self.boundaries[2] <= theta < self.boundaries[1]:
            return mag, 0, 0

        # / red
        if self.boundaries[1] <= theta < self.boundaries[0]:
            return 0, 0, mag


    def run_edges(self, image):
        """ This function finds and colors all edges in the given image.
        """

        # Convert image to gray
        if len(image.shape) > 2:
            grayimage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        else:
            grayimage = image

        # blur so the gradient operation is less noisy.
        # uses a gaussian filter with sigma = 2
        grayimage = gaussian_filter(grayimage, 2).astype(float)

        # Filter with x and y sobel filters
        dx = convolve2d(grayimage, self.sobel_x)
        dy = convolve2d(grayimage, self.sobel_y)

        # Convert to orientation and magnitude images
        theta = self.transform_xy_theta(dx, dy)
        mag = self.transform_xy_mag(dx, dy)

        outimg = np.zeros((image.shape[0], image.shape[1], 3), dtype=np.uint8)

        # Fill with corresponding color.
        for r in range(outimg.shape[0]):
            for c in range(outimg.shape[1]):
                outimg[r, c, :] = self.get_color(theta[r, c], mag[r, c])

        return outimg

    def split_rgb(self, image):
        """
        Split the target image into its red, green and blue channels.

        image - a numpy array of shape (rows, columns, 3).
        output - three numpy arrays of shape (rows, columns) and dtype same as
             image, containing the corresponding channels.

        Please make sure the output shape has only 2 components!
        For instance, (600, 800) instead of (600, 800, 1)
        """
        blue = image[:, :, 0]
        green = image[:, :, 1]
        red = image[:, :, 2]

        return red, green, blue

    def get_contours(self, img, mode, method):
        edges = self.canny_filter(img)
        contours, hierarchy = cv2.findContours(edges, mode, method)
        return contours

    def get_contour_area(self, contour):
        return cv2.contourArea(contour)

    def grayscale(self, img):
        return cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    def gauss_blur(self, img):
        return cv2.GaussianBlur(img, (7, 7), 2)


    def gauss_blur_grayscale(self, img):
        return self.gauss_blur(self.grayscale(img))

    def canny_filter(self, img):
        smoothedInput = self.gauss_blur_grayscale(img)
        return cv2.Canny(smoothedInput, 25, 50)


    def exec_contour_area(self, img):
        out_img = img.copy()
        contours = self.get_contours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        for h, contour in enumerate(contours):
            area = self.get_contour_area(contour)
            print area
        return out_img

    def exec_find_contours(self, img, show=False):
        out_img = img.copy()
        contours = self.get_contours(img, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

        if len(contours) != self.last_contour_count:
            self.last_contour_count = len(contours)
        if show:
            for h, cnt in enumerate(contours):
                color = np.random.randint(0, 255, 3).tolist()
                cv2.drawContours(out_img, [cnt], 0, color, -1)
        return out_img, contours

    def exec_find_contours_mod(self, img, show=False):
        out_img = img.copy()
        contours = self.get_contours(img, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

        if len(contours) != self.last_contour_count:
            self.last_contour_count = len(contours)

        if show:
            big_contours = 0
            for h, contour in enumerate(contours):
                if self.get_contour_area(contour) > 2000:
                    big_contours += 1
                    color = np.random.randint(0, 255, 3).tolist()
                    cv2.drawContours(out_img, [contour], 0, color, -1)

        return out_img, contours

    def exec_goodFeaturesToTrack_filter(self, img, show=False):
        out_img = img.copy()
        input_img = img.copy()
        features = None
        try:
            canny = self.canny_filter(input_img)
            features = cv2.goodFeaturesToTrack(canny, 100, .2, 20)
            if features is not None:
                features = features.reshape((-1, 2))
                if show:
                    for x, y in features:
                        cv2.circle(out_img, (x, y), 10, RED_BGR)
        except Exception as err:
            print err.message
        return out_img, features

    def exec_corner_detection(self, img, bgr_result=True):
        # inspired by http://stackoverflow.com/questions/18255958/harris-corner-detection-and-localization-in-opencv-with-python
        tmp, binary = cv2.threshold(self.grayscale(img), 10, 255, cv2.THRESH_BINARY)
        corner_img = cv2.cornerHarris(binary, 2, 3, 0.04)
        corner_img = cv2.normalize(corner_img, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_32FC1, None)
        corner_img = cv2.convertScaleAbs(corner_img)

        retval, corner_img = cv2.threshold(corner_img, 64, 255, cv2.THRESH_TOZERO)

        if bgr_result:
            return cv2.cvtColor(corner_img, cv2.COLOR_GRAY2BGR)
        else:
            return corner_img

    def exec_surf_matching(self, img, shape):
        surf = cv2.SURF()
        gray = self.grayscale(img)
        keys, desc = surf.detect(gray, None, useProvidedKeypoints=False)

        for h, des in enumerate(desc):
            des = np.array(des, np.float32).reshape((1, 128))
            retval, results, neigh_resp, dists = shape.knn.find_nearest(des, 1)
            res, dist = int(results[0][0]), dists[0][0]

            if dist < 0.1: # draw matched keypoints in red color
                color = (0, 0, 255)
            else:  # draw unmatched in blue color
                print dist
                color = (255, 0, 0)

            #Draw matched key points on original image
            x, y = shape.kp[res].pt
            center = (int(x), int(y))
            cv2.circle(img, center, 2, color, -1)
        return img

    def exec_houghCircles_filter(self, img, show=False):
        out_img = img.copy()
        smoothedInput = self.gauss_blur_grayscale(img)
        circles = cv2.HoughCircles(smoothedInput, cv2.cv.CV_HOUGH_GRADIENT, 1, 10, param1=100, param2=30, minRadius=1,
                                   maxRadius=100)
        if circles is not None and show:
            for i in circles[0, :]:
                cv2.circle(out_img, (i[0], i[1]), i[2], GREEN_BGR, 1)  # outline of circle
                cv2.circle(out_img, (i[0], i[1]), 2, RED_BGR, 3)     # center of circle
        return out_img, circles

    def exec_houghLines_filter(self, img, show=False):
        out_img = img.copy()
        smoothedInput = self.gauss_blur_grayscale(img)
        canny = cv2.Canny(smoothedInput, 25, 50)
        lines = cv2.HoughLines(canny, 5, np.pi / 90, 150)
        if lines is not None and show:
            for rho, theta in lines[0]:
                x0 = np.cos(theta) * rho
                y0 = np.sin(theta) * rho
                p1 = (int(np.around(x0 + 1000 * (-np.sin(theta)))), int(np.around(y0 + 1000 * (np.cos(theta)))))
                p2 = (int(np.around(x0 - 1000 * (-np.sin(theta)))), int(np.around(y0 - 1000 * (np.cos(theta)))))
                cv2.line(out_img, p1, p2, GREEN_BGR, 2)
        return out_img, lines

    def exec_houghLinesP_filter(self, img, show=False):
        out_img = img.copy()
        canny = self.canny_filter(img)
        lines = cv2.HoughLinesP(canny, 1, math.pi / 90, 3, None, 10, 1)
        if lines is not None and show:
            for x1, y1, x2, y2 in lines[0]:
                cv2.line(out_img, (x1, y1), (x2, y2), GREEN_BGR, 2)
        return out_img, lines

    def exec_shape_match(self, img, show=False):
        polys = []
        if img is not None:
            out_img = np.ones(img.shape, dtype=np.uint8)
            contours = self.get_contours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

            for h, contour in enumerate(contours):
                if self.get_contour_area(contour) > 2000:
                    new_s = np.array(contour[:, 0])
                    appr_s = approximate_polygon(new_s, tolerance=5)
                    polys.append(appr_s)

                    if show:
                        cv2.drawContours(out_img, [appr_s], 0, GRAY, -1)
        else:
            out_img = img
        return out_img, polys

    def calc_bounding_box(self, points):
        x_vals = []
        y_vals = []
        for point in points:
            x_vals.append(point[0])
            y_vals.append(point[1])

        return min(x_vals), max(x_vals), min(y_vals), max(y_vals)

    def calc_hist(self, img, mask=None):
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        return cv2.calcHist([hsv], [0, 1], mask, [180, 256], [0, 180, 0, 256])

    def calc_hist_in_shape_outside_area(self, img, shape, poly):
        mask = np.zeros(img.shape[0:2], dtype="uint8")
        cv2.fillConvexPoly(mask, np.array([shape], 'int32'), 255)
        cv2.fillConvexPoly(mask, np.array([poly], 'int32'), 0)
        return self.calc_hist(img, mask=mask)

    def calc_hist_in_area(self, img, poly):
        mask = np.zeros(img.shape[0:2], dtype="uint8")
        cv2.fillConvexPoly(mask, np.array([poly], 'int32'), 255)
        return self.calc_hist(img, mask=mask)

    def calc_avg_color_in_area(self, img, poly):
        mask = np.zeros(img.shape[0:2], dtype="uint8")
        cv2.fillConvexPoly(mask, np.array([poly], 'int32'), 255)
        mean = cv2.mean(img, mask=mask)
        return mean[0:len(poly)]

    def compare_hists(self, h0, h1):
        match = cv2.compareHist(h0, h1, cv2.cv.CV_COMP_CORREL)
        return match

    def compare_colors(self, color0, color1):
        """
        see https://de.wikipedia.org/wiki/Delta_E
        0,0 … 0,5	kein bis fast kein Unterschied
        0,5 … 1,0	Unterschied kann für das geübte Auge bemerkbar sein
        1,0 … 2,0	merklicher Farbunterschied
        2,0 … 4,0	wahrgenommener Farbunterschied
        4,0 … 5,0	wesentlicher Farbunterschied, der selten toleriert wird
        oberhalb 5,0	die Differenz wird als andere Farbe bewertet
        """
        c0 = RGBColor(color0[0], color0[1], color0[2])
        c1 = RGBColor(color1[0], color1[1], color1[2])

        return c0.delta_e(c1, mode='cie1976')



