from unittest import TestCase

import numpy as np
import cv2

from image_operations import Image_operations


__author__ = 'Alexander'


class TestImage_operations(TestCase):
    def test_calc_bounding_box(self):
        ip = Image_operations()
        min_x, max_x, min_y, max_y = ip.calc_bounding_box([(0, 0), (1, 4), (2, -1), (3, 3)])

        self.assertEqual(min_x, 0)
        self.assertEqual(max_x, 3)
        self.assertEqual(min_y, -1)
        self.assertEqual(max_y, 4)

    def test_calc_hist_in_area(self):
        ip = Image_operations()

        p0 = (0, 0)
        p1 = (10, 10)
        p2 = (8, 2)
        poly = [p0, p1, p2]

        img_black = np.zeros((20, 20, 3))
        cv2.fillConvexPoly(img_black, np.array(poly, 'int32'), (0, 0, 0))

        color = (255, 255, 255)
        img_color = np.zeros((20, 20, 3))
        cv2.fillConvexPoly(img_color, np.array(poly, 'int32'), color)

        hist_black = ip.calc_hist_in_area(img_black, poly)
        hist_color = ip.calc_hist_in_area(img_color, poly)

        match = ip.compare_hists(hist_black, hist_color)
        print match

        match = ip.compare_hists(hist_color, hist_color)
        print match

        match = ip.compare_hists(hist_black, hist_black)
        print match


    def test_compare_colors(self):
        ip = Image_operations()

        color0 = (255, 255, 0)
        res = ip.compare_colors(color0, color0)
        self.assertTrue(res == 0)

        color0 = (255, 255, 0)
        color1 = (255, 254, 0)
        res = ip.compare_colors(color0, color1)
        self.assertTrue(res < 2)

        color0 = (230, 230, 10)
        color1 = (255, 255, 0)
        res = ip.compare_colors(color0, color1)
        self.assertTrue(5 < res < 20)

        color0 = (255, 0, 0)
        color1 = (255, 255, 0)
        res = ip.compare_colors(color0, color1)
        self.assertTrue(res > 20)