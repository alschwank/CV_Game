import pickle
from random import shuffle, sample, choice

import cv2
import numpy as np

from vector import unit_vector
from constants import *
import image_operations
import shape_generation


__author__ = 'Alexander'


class Shape:
    contour = None
    img = None
    color = None

    def __init__(self):
        self.img = np.zeros((TOWER_SIZE, TOWER_SIZE, 3), dtype=np.uint8)
        self.img_ops = image_operations.Image_operations()

    def generate_random_shape(self, color=None, edgecount=4):
        if color is None:
            self.color = np.random.randint(0, 255, 3).tolist()
        else:
            self.color = color
        self.triangle_color = GREEN_BGR
        self.points, self.angles = shape_generation.TwoPeasants().generate(self.img.shape[0], edgecount)
        self.init_area_info()
        self.init_color_triangle()
        self.generate_img_by_polygon(self.points)
        self.init_contour()
        self.init_lengths()
        self.corner_img = self.img_ops.exec_corner_detection(self.img)

    def generate_by_points(self, points, color=None):
        self.points = points
        if color is None:
            self.color = np.random.randint(0, 255, 3).tolist()
        else:
            self.color = color
        self.triangle_color = GREEN_BGR
        self.init_angles()
        self.init_area_info()
        self.init_color_triangle()
        self.generate_img_by_polygon(self.points)
        self.init_contour()
        self.init_lengths()
        self.corner_img = self.img_ops.exec_corner_detection(self.img)

    def init_contour(self):
        gray = cv2.cvtColor(self.img, cv2.COLOR_BGR2GRAY)
        contours, hierarchy = cv2.findContours(gray, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        self.contour = contours[0]

    def generate_img_by_polygon(self, poly, fill=False):
        if fill:
            cv2.fillConvexPoly(self.img, np.array(poly, 'int32'), self.color)
        else:
            if self.triangle is not None:
                cv2.fillConvexPoly(self.img, np.array(self.triangle, 'int32'), self.triangle_color)
            cv2.drawContours(self.img, np.array([poly], 'int32'), 0, self.color, thickness=3)
            #self.img =

    def angle_between(self, a, b):
        # based on http://stackoverflow.com/questions/2827393/angles-between-two-n-dimensional-vectors-in-python
        """ Returns the angle in radians between vectors 'v1' and 'v2'::

                angle_between((1, 0, 0), (0, 1, 0))
                1.5707963267948966
                angle_between((1, 0, 0), (1, 0, 0))
                0.0
                angle_between((1, 0, 0), (-1, 0, 0))
                3.141592653589793
        """
        v0 = a[1] - a[0]
        v1 = b[1] - b[0]
        v0_u = unit_vector(v0)
        v1_u = unit_vector(v1)
        angle = np.arccos(np.dot(v0_u, v1_u))
        if np.isnan(angle):
            if (v0_u == v1_u).all():
                return 0.0
            else:
                return 360
        return np.degrees(angle)

    def init_angles(self):
        self.angles = []
        end_eq_start = self.points[0][0] == self.points[len(self.points) - 1][0] \
            and self.points[0][1] == self.points[len(self.points) - 1][1]
        for i in range(0, len(self.points) - 1):
            if i == (len(self.points) - 2) and end_eq_start:
                angle = abs(self.angle_between([self.points[i], self.points[0]], [self.points[0], self.points[1]]))
            else:
                angle = abs(self.angle_between([self.points[i], self.points[i + 1]],
                                               [self.points[i + 1], self.points[(i + 2) % len(self.points)]]))
            self.angles.append(angle)

    def calc_triangle_middle(self, triangle):
        psum = [0, 0]
        for point in triangle:
            psum[0] += point[0]
            psum[1] += point[1]
        return psum[0] / 3, psum[1] / 3

    def triangle_is_valid(self, start):
        # check if middle point of triangle is inside polygon. otherwise triangle is invalid
        middle = self.calc_triangle_middle(self.triangle)
        if cv2.pointPolygonTest(np.array([self.points], 'int32'), middle, False) < 0:
            return False

        # check if other points pf polygon are not inside triangle. otherwise triangle is invalid
        for i, point in enumerate(self.points):
            # ignore points of triangle
            if i < start or i > start + 1:
                if cv2.pointPolygonTest(np.array([self.triangle], 'int32'), (point[0], point[1]), False) > 0:
                    return False

        # check if area of triangle is not too big and not too small for polygon
        area = cv2.contourArea(np.array(self.triangle, 'int32'))
        return MIN_COLOR_TRIANGLE_AREA < area / self.area < MAX_COLOR_TRIANGLE_AREA

    def init_color_triangle(self):
        starts = range(len(self.points))
        if SHUFFLE_COLOR_TRIANGLE_INDEX:
            shuffle(starts)
        for start in starts:
            self.triangle = [self.points[start], self.points[(start + 1) % len(self.points)],
                             self.points[(start + 2) % len(self.points)]]
            if self.triangle_is_valid(start):
                self.triangle_start = start
                return
        self.triangle = None

    def init_area_info(self):
        self.area = cv2.contourArea(np.array([self.points], 'int32'))

    def init_lengths(self):
        self.lengths = []
        for i in range(0, len(self.points) - 1):
            point = self.points[i]
            other = self.points[(i + 1) % len(self.points)]
            length = np.sqrt(np.power(point[0] - other[0], 2) + np.power(point[1] - other[1], 2))
            self.lengths.append(length)


class Shape_Collection():
    def __init__(self, difficulty=0):
        self.pkls = []
        self.shapes = []
        if difficulty == 2:
            self.filename_number = 5
        else:
            self.filename_number = 4

    def serialize(self):
        loaded_shapes = []
        for i in range(0, 100):
            print "generating shape #", i
            s = Shape()
            got_shape = False
            while not got_shape:
                try:
                    s.generate_random_shape(edgecount=self.filename_number)
                    got_shape = True
                except IndexError:
                    pass
            loaded_shapes.append(s)
        filename = SHAPES_FILENAME_PARTS[0] + str(self.filename_number) + SHAPES_FILENAME_PARTS[1]
        with open(filename, "wb") as pkl_file:
            data = []
            for shape in loaded_shapes:
                data.append(shape.points)
            pickle.dump(data, pkl_file)

    def color_by_index(self, index):
        color = None
        if index == EARTH['id']:
            color = BGR_COLOR_EARTH
        elif index == FIRE['id']:
            color = BGR_COLOR_FIRE
        elif index == WATER['id']:
            color = BGR_COLOR_WATER
        return color

    def deserialize(self):
        filename = SHAPES_FILENAME_PARTS[0] + str(self.filename_number) + SHAPES_FILENAME_PARTS[1]
        with open(filename, "rb") as pkl_file:
            self.pkls = pickle.load(pkl_file)

        for i, points in enumerate(sample(self.pkls, TOWER_COUNT)):
            s = Shape()
            s.generate_by_points(points, color=self.color_by_index(i))
            self.shapes.append(s)

    def replace_shape(self, shape=None, index=None):
        if index is None:
            index = self.shapes.index(shape)
        points = choice(self.pkls)
        s = Shape()
        s.generate_by_points(points, color=self.color_by_index(index))
        self.shapes[index] = s

    def get_combined_img(self, img_type='original', rotate=False):
        h = TOWER_SIZE
        w = TOWER_SIZE
        all_shapes = np.zeros((h, len(self.shapes) * w, 3), np.uint8)
        for index in range(0, len(self.shapes)):
            s = self.shapes[index]
            used_img = None
            if img_type == "original":
                used_img = cv2.flip(s.img, 1)
            elif img_type == "corners":
                used_img = s.corner_img
            all_shapes[:TOWER_SIZE, (index * w):((index + 1) * w), :] = used_img

        if rotate:
            all_shapes = np.rot90(all_shapes)
        return all_shapes