import random
import math

import pygame

from stats import *
from stats import Tower_Stats
from snap_to_grid import *
from object_groups import *
from vector import *
from constants import WIDTH, HEIGHT, MAP_MARGIN, DOWN, UP, LEFT, RIGHT


class Tower(pygame.sprite.Sprite):
    def __init__(self, position, tower_type):
        self.groups = towers, all_update, all_draw, all_objects # same as above
        pygame.sprite.Sprite.__init__(self, self.groups)

        self.current_angle = 0

        self.tower_type = tower_type
        self.stats = Tower_Stats(tower_type)
        self.orginal_image = self.stats['image']
        self.image = self.orginal_image # holds rotated im  age
        self.rotatable = self.stats['rotatable'] # can the tower be rotated?
        self.range = self.stats['radius'] # range in pixels
        self.speed = self.stats['speed'] # time between shots
        self.targeting_type = self.stats['targeting'] # useful for targeting
        self.bullet_limit = self.stats['bullet_limit']
        self.shoots_at_front = self.stats['shoots_at_front']
        self.damage = self.stats['bullet_damage']
        self.rect = self.image.get_rect() # get it, just get it
        self.upgrade_counter = 0

        grid_position = snap_to_grid(position)
        self.rect.center = grid_position # where shall i go dear slavelord
        self.shooting_timer = 0 # start at 0 so we are loaded when tower is placed
        self.current_target = 0 # the target is the one FIX THIS
        self.target = None # actual target
        self.shots = []

    def get_rotation_direction(self):
        """
        Function:
            gets direction to face
        """
        x = vec2d(self.rect.centerx, self.rect.centery) # define where the shot is located at (x, y)
        y = vec2d(self.target.rect.centerx, self.target.rect.centery) # define the target location (x, y)
        self.total_dist = y - x # calculate total distance from shot to target using above values
        radians = math.atan2(self.total_dist[1], self.total_dist[0]) # convert vector to radians
        degrees = radians / 0.0174532 #convert to degrees
        degrees *= -1

        return degrees

    def rotate(self):
        self.old_center = self.rect.center
        self.get_rotation_direction()
        self.image = pygame.transform.rotate(self.orginal_image, self.get_rotation_direction())
        self.rect = self.image.get_rect()
        self.rect.center = self.old_center

    def shoot(self, target):

        self.current_angle += 137.5
        current_radians = self.current_angle * 0.0174532
        radians = current_radians

        self.target = target
        self.shooting_timer = self.speed # reset the timer
        self.shot = Shot(self.rect.center, self.target, self.tower_type, radians, self.damage, self.speed,
                         self.range) # creates a shot at turret location

        if self.rotatable: # if we can rotate the image
            self.rotate() # rotate towards the target

        if self.targeting_type == 'orbital':
            self.shots.append(self.shot)
            #music.play(music.channel_basic, music.basic_shot, False)

    def upgrade(self):
        stats = Upgrade_Stats()
        self.range *= stats.range
        self.speed *= stats.speed
        self.damage *= stats.damage
        self.upgrade_counter += 1

    def update_shot_list(self): #clears dead shots from the list
        for s in self.shots:
            if not s.is_alive:
                self.shots.remove(s) # if dead remove from shots group


class Manual_Shot(pygame.sprite.Sprite):
    def __init__(self, position):
        self.groups = manual_shot_group, all_draw, all_objects
        pygame.sprite.Sprite.__init__(self, self.groups)

        self.stats = Tower_Stats(MANUAL_SHOT)
        self.image = self.stats['image']

        self.image.set_alpha(175) # make the image see through 255 is most visible
        self.rect = self.image.get_rect()

        grid_position = snap_to_grid(position) # put the position in the grid form
        self.rect.center = grid_position

        self.radius = self.stats['radius'] # get the radius of the tower
        self.damage = self.stats['bullet_damage']


    def move(self, position):
        self.rect.center = min(position[0], 825), min(position[1], 575)

    def shot(self, click, enemies):
        for enemy in enemies:
            distance = math.sqrt(
                math.pow(click[0] - enemy.rect.center[0], 2) + math.pow(click[1] - enemy.rect.center[1], 2))
            if distance < self.radius:
                enemy.get_hit(self.damage)


class TowerPlacer(pygame.sprite.Sprite):
    def __init__(self, position, tower_type): # gets location and the kind of tower
        self.groups = towerplacers, all_draw, all_objects
        pygame.sprite.Sprite.__init__(self, self.groups)

        self.tower_type = tower_type
        self.image = Tower_Stats(tower_type)['image'] # get the image

        self.image.set_alpha(175) # make the image see through 255 is most visible
        self.rect = self.image.get_rect()

        grid_position = snap_to_grid(position) # put the position in the grid form
        self.rect.center = grid_position

        self.radius = Tower_Stats(tower_type)['radius'] # get the radius of the tower

    def move(self, position):
        p = snap_to_grid(position)
        self.rect.center = min(p[0], 825), min(p[1], 575)

##### Range circle #####
class Tower_Range(pygame.sprite.Sprite):
    def __init__(self, location, tower_range):
        self.groups = tower_range_group, all_draw, all_objects
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.range = tower_range
        self.image = pygame.Surface((self.range * 2, self.range * 2)) # create a box surface
        self.image.fill((255, 255, 255)) # fill the image with RGB
        self.image.set_colorkey(self.image.get_at((0, 0))) # make the white transparent
        self.rect = self.image.get_rect()
        self.rect.center = location # draw it at the location
        pygame.draw.circle(self.image, (0, 200, 0), ((int(self.range), int(self.range))), int(self.range),
                           2) # draw the circle inside the box

    def move(self, position):
        p = snap_to_grid(position)
        self.rect.center = min(p[0], 825), min(p[1], 575)


class Shot(pygame.sprite.Sprite):
    def __init__(self, position, target, tower_type, current_angle, damage, speed, radius):
        self.groups = shots, all_draw, all_objects, all_update
        pygame.sprite.Sprite.__init__(self, self.groups)

        self.current_angle = current_angle

        self.position = position
        self.target = target # target we are shooting at
        self.current_target = -1 # used to get a new target
        self.target_test = None # holds what target we are testing
        self.tower_type = tower_type
        self.is_alive = True

        self.damage = damage
        self.range = radius
        self.speed = speed
        self.stats = Tower_Stats(self.tower_type)
        self.image = self.stats['attack_image'] # the image of the shot
        self.targeting_type = self.stats['targeting'] # what targeting does the shot use
        self.multiple_targets = self.stats['multiple_targets']
        self.bullet_limit = self.stats['bullet_limit']
        self.shot_error = self.stats['shot_error']
        self.hit_target_only = self.stats['hit_target_only'] # can the shot hit others besides the target
        self.hit_kills_shot = self.stats['hit_kills_shot'] # does the shot die after hitting an enemy
        self.rotatable_shot = self.stats['rotatable_shot']
        self.extra_timer = self.stats['timer'] # timer for targeting
        self.extra_timer1 = self.stats['timer1'] # extra timer between attacks (bee)
        self.beam_color = self.stats['beam_color']
        self.beam_width = self.stats['beam_width']
        self.has_splash_attacks = self.stats['has_splash_attacks']
        self.radius = self.stats['splash_radius'] # radius of the splash attack
        self.splash_damage = self.stats['splash_damage']
        self.has_slowing_attacks = self.stats['has_slowing_attacks']
        self.slow_percentage = self.stats['slow_percentage']
        self.slow_duration = self.stats['slow_duration']

        self.distance_traveledX = 0 # we have not traveled anywhere yet
        self.distance_traveledY = 0
        self.total_distance_traveled = 0
        self.timer = 0 # starts at 0
        self.timer1 = 0 # starts at 0
        self.direction_change = 0 # so we can reset the value
        self.speedX = 0 # the speed in x/y directions
        self.speedY = 0
        self.alpha_value = 255
        self.image_width = None
        self.image_height = None
        self.damage_done = False # we have not done damage with this shot yet

        self.direction = self.first_shot_direction()

        if self.targeting_type == 'beam':
            self.image_width = self.total_dist[0]
            self.image_height = self.total_dist[1]

            if not self.image_width:
                self.image_width = self.beam_width
            if not self.image_height:
                self.image_height = self.beam_width

            self.image = pygame.Surface((math.fabs(self.image_width), math.fabs(self.image_height)))
            self.image.fill((255, 255, 255)) # fill the image with RGB
            self.image.set_colorkey((255, 255, 255)) # make the white transparent

        self.rect = self.image.get_rect()
        self.rect.center = position
        self.trueX = self.rect.centerx # create true stats to hold decimal values
        self.trueY = self.rect.centery

        self.total_dist = None
        self.RAND_DIR_TIMER = self.extra_timer
        self.rand_dir_timer = 100 # random direction timer

    def destroy(self):
        self.kill()
        self.is_alive = False

    def first_shot_direction(self):
        # for when the start is not the center of the tower
        """
        Function:
            Calculates shot direction using vectors from the current
            shot location to the target

            the direction is then normalized(divided by its length)
            to get a constant speed in any direction

            the direction is then returned to be used for movement of
            a sprite
        """
        x = vec2d(self.position[0], self.position[1]) # define where the shot is located at (x, y)
        y = vec2d(self.target.rect.centerx, self.target.rect.centery) # define the target location (x, y)
        self.total_dist = y - x # calculate total distance from shot to target using above values
        direction = self.total_dist.normalize() # normalizes the distance so we only move one pixel per update (* speed)

        if self.shot_error: # if the shot has a shot error variable
            dist_x = self.total_dist[0] # gets the x from the dist tuple
            dist_y = self.total_dist[1] # gets the y from the dist tuple
            radians = math.atan2(dist_y, dist_x) # convert vector to radians

            radians += ((random.random() * self.shot_error) * random.choice([-1, 1])) # apply randomness
            direction_x = math.cos(radians) # convert back to a vector
            direction_y = math.sin(radians) # convert back to a vector
            direction = (direction_x, direction_y)

        return direction # send the stats

    def shot_direction(self): # used to get the direction to move.
        """
        Function:
            Calculates shot direction using vectors from the current
            shot location to the target

            the direction is then normalized(divided by its length)
            to get a constant speed in any direction

            the direction is then returned to be used for movement of
            a sprite
        """

        if self.targeting_type == 'homing' or 'cruise' or 'orbital':
            x = vec2d(self.rect.centerx, self.rect.centery) # define where the shot is located at (x, y)
            y = vec2d(self.target.rect.centerx, self.target.rect.centery) # define the target location (x, y)
            self.total_dist = y - x # calculate total distance from shot to target using above values
            direction = self.total_dist.normalize() # normalizes the distance so we only move one pixel per update (* speed)

            if self.shot_error: # if the shot has a shot error variable
                dist_x = self.total_dist[0] # gets the x from the dist tuple
                dist_y = self.total_dist[1] # gets the y from the dist tuple
                radians = math.atan2(dist_y, dist_x) # convert vector to radians
                radians += ((random.random() * self.shot_error) * random.choice([-1, 1])) # apply randomness
                direction_x = math.cos(radians) # convert back to a vector
                direction_y = math.sin(radians) # convert back to a vector
                direction = (direction_x, direction_y)

        return direction # send the stats


    def get_rotation_direction(self, direction):
        """
        Function:
            gets direction to face
        """

        dist_x = direction[0] # gets the x from the dist tuple
        dist_y = direction[1] # gets the y from the dist tuple
        radians = math.atan2(dist_y, dist_x) # convert vector to radians
        degrees = radians / 0.0174532 #convert to degrees
        degrees *= -1

        return degrees

    def rotate_towards_target(self, direction):
        self.old_center = self.rect.center
        self.image = pygame.transform.rotate(self.orginal_image, self.get_rotation_direction(direction))
        self.rect = self.image.get_rect()
        self.rect.center = self.old_center


    def get_new_target(self, enemies):

        if len(enemies) > 0: # if there are enemies on the screen
            return enemies.get_sprite(0)
        else:
            return None

    def get_line_positions(self, enemy_position, position, image_width, image_height):

        """
        this gets the start of the line and the end of the line
        that will be used in drawing the line from the tower to the
        enemy.
        this is needed because we are drawing the lines on the local
        surface instead of the entire screen. So we must use local coordinates
        """
        if self.rect.centerx < enemy_position[0]:
            if self.rect.centery < enemy_position[1]:
                #then its topleft
                self.rect.topleft = position
                line_start_position = (0, 0)
                line_end_position = (image_width, image_height)

            elif self.rect.centery > enemy_position[1]:
                #then its botleft
                self.rect.bottomleft = position
                line_start_position = (0, image_height)
                line_end_position = (image_width, 0)

            elif self.rect.centery == enemy_position[1]:
                self.rect.midleft = position
                line_start_position = (0, (image_height / 2))
                line_end_position = (image_width, (image_height / 2))

        elif self.rect.centerx > enemy_position[0]:
            if self.rect.centery < enemy_position[1]:
                #then its topright
                self.rect.topright = position
                line_start_position = (image_width, 0)
                line_end_position = (0, image_height)

            elif self.rect.centery > enemy_position[1]:
                #then its botright
                self.rect.bottomright = position
                line_start_position = (image_width, image_height)
                line_end_position = (0, 0)

            elif self.rect.centery == enemy_position[1]:
                #then its midright
                self.rect.midright = position
                line_start_position = (image_width, (image_height / 2))
                line_end_position = (0, (image_height / 2))

        elif self.rect.centerx == enemy_position[0]:
            if self.rect.centery > enemy_position[1]:
                #then its midbottom
                self.rect.midbottom = position
                line_start_position = ((image_width / 2), image_height)
                line_end_position = ((image_width / 2), 0)

            elif self.rect.centery < enemy_position[1]:
                #then its midtop
                self.rect.midtop = position
                line_start_position = ((image_width / 2), 0)
                line_end_position = ((image_width / 2), image_height)

            elif self.rect.centery == enemy_position[1]:
                #then the enemy is directly on top of the tower
                self.rect.center = position
                line_start_position = ((image_width / 2), (image_height / 2))
                line_end_position = ((image_width / 2), (image_height / 2))

        return line_start_position, line_end_position


    def move(self, player): # move in a certain direction
        # boundaries
        if (self.rect.left < (0 - (TILE_SIZE * 4)) or
                    self.rect.right > (WIDTH + (TILE_SIZE * 4)) or
                    self.rect.top < (0 - (TILE_SIZE * 4)) or
                    self.rect.bottom > (HEIGHT + (TILE_SIZE * 4))):
            self.destroy()
        else:

            ##### collisions with enemies #####
            if len(enemies) >= 1:

                if self.damage != 0: # if the shot actually hurts stuff
                    if self.hit_target_only: # if it only hits its target
                        if self.timer1 is not None: # if there is a delay between attacks (bee)
                            self.timer1 -= 1 # countdown until next attack
                            if self.timer1 <= 0: # if its time to attack
                                self.timer1 = self.extra_timer1 # reset the attack timer
                                hitTarget = pygame.sprite.collide_rect(self, self.target)
                                if hitTarget:
                                    self.target.get_hit(self.damage, player)
                                    if self.hit_kills_shot:
                                        self.destroy()
                        else: # if there is no delay between attacks
                            hitTarget = pygame.sprite.collide_rect(self, self.target)
                            if hitTarget:
                                self.target.get_hit(self.damage)
                                if self.hit_kills_shot:
                                    self.destroy()

                    else: # if it can hit any enemy
                        enemy_hit = pygame.sprite.spritecollideany(self, enemies)
                        if enemy_hit:
                            enemy_hit.get_hit(self.damage)
                            if self.hit_kills_shot:
                                self.destroy()
                            if self.has_splash_attacks: # if they do splash damage
                                for e in enemies:
                                    splash_collision = pygame.sprite.collide_circle(self, e)
                                    if splash_collision: # if enemies are in range of the splash damage
                                        e.get_hit(self.splash_damage)
                            if self.has_slowing_attacks:
                                enemy_hit.get_slowed(self.slow_percentage, self.slow_duration, player)

            self.trueX += (self.direction[0] * self.speed)
            self.trueY += (self.direction[1] * self.speed)

            self.rect.center = (round(self.trueX), round(self.trueY))


class TowerUpgrader(pygame.sprite.Sprite):
    def __init__(self, position):
        self.groups = tower_upgrader, all_draw, all_objects
        pygame.sprite.Sprite.__init__(self, self.groups)

        self.image = pygame.image.load(
            os.path.join('Resources', 'Images', 'Sprites', 'Towers', 'upgrade.png')).convert_alpha()
        self.image.set_alpha(175)
        self.rect = self.image.get_rect()

        grid_position = snap_to_grid(position) # put the position in the grid form
        self.rect.center = grid_position

        self.radius = 100

    def move(self, position):
        grid_position = snap_to_grid(position)
        self.rect.center = grid_position


class Life_Bar(pygame.sprite.Sprite):
    """draws a life bar over the sprites head
    """

    def __init__(self, starting_life, life, position, height, width):
        self.groups = life_bars, all_objects
        pygame.sprite.Sprite.__init__(self, self.groups)

        self.bar_height = 4
        self.bar_width = 16
        self.health_percent = starting_life / life

        self.image = pygame.Surface((self.bar_width, self.bar_height))
        self.image.fill((0, 255, 0))
        self.rect = self.image.get_rect()
        self.host_height = height
        self.host_width = width

        self.rect.center = (position[0] - (width / 4)), (position[1] - height)
        self.starting_life = starting_life

    def is_dead(self):
        self.kill()

    def update(self, starting_life, life, position):
        self.health_percent = starting_life / float(life)

        self.image.fill((255, 0, 0)) # fill with red
        self.image.fill((0, 255, 0), # green
                        (0, 0, int(self.bar_width / self.health_percent), self.bar_height)) # size of green

        self.rect.center = position[0], (position[1] - self.host_height)


class Enemy(pygame.sprite.Sprite):
    def __init__(self, player, wave):
        self.groups = enemies, all_update, all_objects  # auto add Enemy to enemies group
        pygame.sprite.Sprite.__init__(self, self.groups)

        self.enemy_stats = enemy_stats(wave.wave_type)

        self.images = self.enemy_stats['image']
        self.base_life = self.enemy_stats['base_life']
        self.speed = self.enemy_stats['speed']
        self.reward = self.enemy_stats['reward']
        self.orginal_speed = self.speed

        self.frame = 0 # starting image
        self.ANIMATION_TIMER = 10
        self.animation_timer = self.ANIMATION_TIMER
        self.animated_image = isinstance(self.images, list) # find if the image needs to be animated True/False

        if self.animated_image:
            self.image_right = self.images[self.frame]
        else:
            self.image_right = self.images
        self.image_up = pygame.transform.rotate(self.image_right, 90)
        self.image_left = pygame.transform.rotate(self.image_right, 180)
        self.image_down = pygame.transform.rotate(self.image_right, 270)

        self.starting_life = (self.base_life * wave.life_multiplier) # used when drawing the life bar
        self.life = self.starting_life

        self.image = self.image_right
        self.rect = self.image.get_rect() # get size of the image so we can use self.rect.center

        # make this more readible
        ##### movement #####
        self.directions = player.game_map.dirs # directions to move gets values from map_directions list based on which map you are on
        self.distances = player.game_map.lengths # how many TILE_SIZEs to move gets values from map_distances list based on which map you are on
        self.start = player.game_map.start # spawn location gets values from map_start list based on which map you are on
        self.current_movement = 0 # to track which move is next
        self.direction = self.directions[self.current_movement] # gets direction from self.directions list
        self.distance = (self.distances[
                             self.current_movement] * TILE_SIZE) # amount of TILE_SIZEs we want to move from the distances list
        self.trueX = (
        (self.start[0] * TILE_SIZE) + (TILE_SIZE / 2)) # calculates starting location in (x,y) using the self.start list
        self.trueY = ((self.start[1] * TILE_SIZE) - (TILE_SIZE / 2))
        self.prevX = self.trueX
        self.prevY = self.trueY
        self.rect.center = (self.trueX, self.trueY)
        self.total_distance_traveled = 0
        self.distance_traveledX = 0
        self.distance_traveledY = 0

        ##### create life bar to display health #####
        self.life_bar = Life_Bar(self.starting_life, self.life, self.rect.center, self.image.get_height(),
                                 self.image.get_width())
        self.is_alive = True
        self.is_being_slowed = False
        self.slowed_timer = None


    def reach_end(self, player):
        self.kill() # kill it!
        player.life -= 1 # hurt player (because letting a enemy get through is bad)
        self.is_alive = False
        self.life_bar.is_dead()

    def is_dead(self):
        self.kill()
        self.life_bar.is_dead()
        self.is_alive = False

    def get_hit(self, damage_of_hit):
        self.life -= damage_of_hit
        if self.life <= 0:
            self.is_dead()

    def get_slowed(self, slowing_percentage, slow_duration):
        if not self.is_being_slowed: # if its not being slowed
            self.is_being_slowed = True
            self.speed *= slowing_percentage
            self.time_until_not_slowed = slow_duration
        else:
            self.time_until_not_slowed = slow_duration

    def update_slowed_state(self):
        # counts down until the enemy is not being slowed anymore
        if self.is_being_slowed:
            self.time_until_not_slowed -= 1
            if self.time_until_not_slowed <= 0:
                self.is_being_slowed = False # were not being slowed anymore
                self.speed = self.orginal_speed

    def update(self, player, first_call=True):

        ##### Directions for movement #####
        if not self.animated_image:
            if self.direction == DOWN: # if direction is down
                self.image = self.image_down # sets the picture to the down image
            if self.direction == UP: # up
                self.image = self.image_up
            if self.direction == LEFT: # left
                self.image = self.image_left
            if self.direction == RIGHT: # right
                self.image = self.image_right

        if self.animated_image: # if there is animation
            self.animation_timer -= 1
            if self.animation_timer <= 0:
                self.animation_timer = self.ANIMATION_TIMER
                if self.frame >= (len(self.images) - 1):
                    self.frame = 0
                else:
                    self.frame += 1
                self.image = self.images[self.frame]


        ##### picking next direction to move #####
        self.distance -= self.speed

        if self.distance <= 0:
            self.current_movement += 1 # next movement

            if self.current_movement >= len(self.directions): # when we are out of directions to move
                self.reach_end(player)

            else: # when there are more directions to move
                self.distance = (
                self.distances[self.current_movement] * TILE_SIZE) # get number of spots to move from the distances list
                self.direction = self.directions[self.current_movement] # get next direction from the directions list

        ##### move the enemy every frame #####
        self.prevX = self.trueX
        self.prevY = self.trueY
        self.distance_traveledX = (
        self.direction[0] * self.speed) # calculate speed from direction to move and speed constant
        self.distance_traveledY = (self.direction[1] * self.speed)
        self.trueX += self.distance_traveledX
        self.trueY += self.distance_traveledY
        self.rect.center = (
        MAP_MARGIN[0] + round(self.trueX), MAP_MARGIN[1] + round(self.trueY)) # apply values to sprite.center

        self.total_distance_traveled += ((self.distance_traveledX ** 2) + (self.distance_traveledY ** 2))

        ##### Transparent #####
        self.image.set_colorkey(self.image.get_at((0, 0)))

        self.life_bar.update(self.starting_life, self.life, self.rect.center) # GET A LIFE!!!

        self.update_slowed_state()

        if player.activated_speed and first_call:
            self.update(player, False)

