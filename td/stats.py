import math
import pygame
from load_sliced_sprites import *
from constants import EARTH, FIRE, WATER, MANUAL_SHOT


class Upgrade_Stats():
    def __init__(self):
        self.range = 1.05
        self.damage = 1.1
        self.speed = 1.05
        self.rect_topleft = (645, 850)
        self.radius = 80


def Tower_Stats(tower_type): # used to get the stats of a certain tower.

    stats = {}

    if tower_type == EARTH:
        stats['description'] = 'Sell and replace quickly to keep the enemies in range.'
        stats['image'] = pygame.image.load(
            os.path.join('Resources', 'Images', 'Sprites', 'Towers', 'earth.png')).convert() # get image
        stats['rotatable'] = True # can the tower rotate to shoot?
        stats['radius'] = 125.0 # range in pixels # MUST HAVE DECIMAL
        stats['speed'] = 25.0 # time between shots # MUST HAVE DECIMAL 25
        stats['attack_image'] = pygame.image.load(
            os.path.join('Resources', 'Images', 'Sprites', 'Ammo', 'bullet green.png')).convert()
        stats['bullet_speed'] = 10 # speed of shots 10
        stats['bullet_damage'] = 5 # read what it said you ratard
        stats['targeting'] = 'cruise' # the attack homes in on the target
        stats['multiple_targets'] = False # does the shot pick a new target after the previous one has died?
        stats['bullet_limit'] = None # how many shots can be on the screen at a time
        stats['shot_error'] = math.radians(3) # how much error in degrees is possible
        stats['distance_till_kill'] = None # how far before the shot self destructs
        stats['hit_target_only'] = False
        stats['hit_kills_shot'] = True # dies after only one hit
        stats['rotatable_shot'] = False # can the shot rotate when its moving?
        stats['timer'] = None # time between update direction
        stats['timer1'] = None
        stats['beam_color'] = None
        stats['beam_width'] = None
        stats['has_splash_attacks'] = False
        stats['splash_radius'] = None
        stats['splash_damage'] = None
        stats['has_slowing_attacks'] = False
        stats['slow_percentage'] = None
        stats['slow_duration'] = None
        stats['shoots_at_front'] = False
        stats['rect_topleft'] = (5, 850)

    if tower_type == FIRE:
        stats['description'] = 'Sell and replace quickly to keep the enemies in range.'
        stats['image'] = pygame.image.load(
            os.path.join('Resources', 'Images', 'Sprites', 'Towers', 'fire.png')).convert() # get image
        stats['rotatable'] = True # can the tower rotate to shoot?
        stats['radius'] = 125.0 # range in pixels # MUST HAVE DECIMAL
        stats['speed'] = 25.0 # time between shots # MUST HAVE DECIMAL 25
        stats['attack_image'] = pygame.image.load(
            os.path.join('Resources', 'Images', 'Sprites', 'Ammo', 'bullet red.png')).convert()
        stats['bullet_speed'] = 10 # speed of shots 10
        stats['bullet_damage'] = 5 # read what it said you ratard
        stats['targeting'] = 'cruise' # the attack homes in on the target
        stats['multiple_targets'] = False # does the shot pick a new target after the previous one has died?
        stats['bullet_limit'] = None # how many shots can be on the screen at a time
        stats['shot_error'] = math.radians(3) # how much error in degrees is possible
        stats['distance_till_kill'] = None # how far before the shot self destructs
        stats['hit_target_only'] = False
        stats['hit_kills_shot'] = True # dies after only one hit
        stats['rotatable_shot'] = False # can the shot rotate when its moving?
        stats['timer'] = None # time between update direction
        stats['timer1'] = None
        stats['beam_color'] = None
        stats['beam_width'] = None
        stats['has_splash_attacks'] = False
        stats['splash_radius'] = None
        stats['splash_damage'] = None
        stats['has_slowing_attacks'] = False
        stats['slow_percentage'] = None
        stats['slow_duration'] = None
        stats['shoots_at_front'] = False
        stats['rect_topleft'] = (217, 850)

    if tower_type == WATER:
        stats['description'] = 'Sell and replace quickly to keep the enemies in range.'
        stats['image'] = pygame.image.load(
            os.path.join('Resources', 'Images', 'Sprites', 'Towers', 'water.png')).convert() # get image
        stats['rotatable'] = True # can the tower rotate to shoot?
        stats['radius'] = 125.0 # range in pixels # MUST HAVE DECIMAL
        stats['speed'] = 25.0 # time between shots # MUST HAVE DECIMAL 25
        stats['attack_image'] = pygame.image.load(
            os.path.join('Resources', 'Images', 'Sprites', 'Ammo', 'bullet blue.png')).convert()
        stats['bullet_speed'] = 10 # speed of shots 10
        stats['bullet_damage'] = 5 # read what it said you ratard
        stats['targeting'] = 'cruise' # the attack homes in on the target
        stats['multiple_targets'] = False # does the shot pick a new target after the previous one has died?
        stats['bullet_limit'] = None # how many shots can be on the screen at a time
        stats['shot_error'] = math.radians(3) # how much error in degrees is possible
        stats['distance_till_kill'] = None # how far before the shot self destructs
        stats['hit_target_only'] = False
        stats['hit_kills_shot'] = True # dies after only one hit
        stats['rotatable_shot'] = False # can the shot rotate when its moving?
        stats['timer'] = None # time between update direction
        stats['timer1'] = None
        stats['beam_color'] = None
        stats['beam_width'] = None
        stats['has_splash_attacks'] = False
        stats['splash_radius'] = None
        stats['splash_damage'] = None
        stats['has_slowing_attacks'] = False
        stats['slow_percentage'] = None
        stats['slow_duration'] = None
        stats['shoots_at_front'] = False
        stats['rect_topleft'] = (430, 850)

    if tower_type == MANUAL_SHOT:
        stats['description'] = 'Sell and replace quickly to keep the enemies in range.'
        # image taken from http://www.ebay.com.au/itm/Sharp-shooter-Flying-Helicopter-Air-Hogs-Battery-Power-Remote-Control-R-C-Plane-/290638271703
        stats['image'] = pygame.image.load(
            os.path.join('Resources', 'Images', 'Sprites', 'Towers', 'target.png')).convert() # get image
        stats['radius'] = 30.0 # range in pixels # MUST HAVE DECIMAL
        stats['speed'] = 25.0 # time between shots # MUST HAVE DECIMAL 25
        stats['bullet_damage'] = 1 # read what it said you ratard
        stats['attack_image'] = pygame.image.load(
            os.path.join('Resources', 'Images', 'Sprites', 'Ammo', 'bullet green.png')).convert()

    stats['image'].set_colorkey(stats['image'].get_at((0, 0))) # transparent
    if stats['attack_image'] is not None:
        stats['attack_image'].set_colorkey(stats['attack_image'].get_at((0, 0))) # transparent

    return stats


def enemy_stats(enemy_type):
    stats = {'base_life': 0}

    if enemy_type == 'basic':
        stats['image'] = pygame.image.load(
            os.path.join('Resources', 'Images', 'Sprites', 'Enemies', 'basic.png')).convert()
        stats['base_life'] = 4
        stats['speed'] = 2.5
        stats['reward'] = 10
        stats['wave_ammount'] = 10 # ammount of enemies per wave

    if enemy_type == 'fast':
        stats['image'] = pygame.image.load(
            os.path.join('Resources', 'Images', 'Sprites', 'Enemies', 'fast.png')).convert()
        stats['base_life'] = 3
        stats['speed'] = 3.5
        stats['reward'] = 10
        stats['wave_ammount'] = 10

    if enemy_type == 'slow':
        stats['image'] = pygame.image.load(
            os.path.join('Resources', 'Images', 'Sprites', 'Enemies', 'slow.png')).convert()
        stats['base_life'] = 8
        stats['speed'] = 1.3
        stats['reward'] = 10
        stats['wave_ammount'] = 10

    if enemy_type == 'boss':
        stats['image'] = pygame.image.load(
            os.path.join('Resources', 'Images', 'Sprites', 'Enemies', 'boss.png')).convert()
        stats['base_life'] = 100
        stats['speed'] = 1.0
        stats['reward'] = 100
        stats['wave_ammount'] = 1

    if enemy_type == 'zombie':
        stats['image'] = load_sliced_sprites(30, 30, os.path.join('Resources', 'Images', 'Sprites', 'Enemies',
                                                                  'zombie tile set.png'))
        stats['base_life'] = 200
        stats['speed'] = 0.7
        stats['reward'] = 200
        stats['wave_ammount'] = 1

    return stats
    

