import cv2
import numpy as np

from input_process import Input_process
from image_operations import Image_operations


__author__ = 'Alexander'


class Cam_Process(Input_process):
    def __init__(self):
        Input_process.__init__(self)

    def calibrate(self):
        img_ops = Image_operations()
        calib_window = cv2.namedWindow("CALIBRATION", cv2.CV_WINDOW_AUTOSIZE)

        self.color_calibration = {"background": None,
                                  "color_triangle": None}

        # find middle
        stream, img = self.read_cam()
        print "shape", img.shape
        x = int(img.shape[1] * 0.5)
        y = int(img.shape[0] * 0.5)
        print x, y
        radius = 20
        detection_poly = [(x + radius, y), (x, y + radius), (x - radius, y), (x, y - radius)]

        mask = np.zeros(img.shape, dtype="uint8")
        cv2.fillConvexPoly(mask, np.array([detection_poly], 'int32'), (1, 1, 1))

        for requested_color in self.color_calibration.keys():
            img = None
            while True:
                stream, img = self.read_cam()
                img *= 0.5
                show_img = img * mask + img
                cv2.putText(show_img, requested_color, (100, 100), cv2.FONT_HERSHEY_PLAIN, 3.0, (255, 0, 0),
                            thickness=2)
                cv2.imshow(calib_window, show_img)

                key = cv2.waitKey(100)
                # enter = calc color now
                if key == 13:
                    break
                    # esc = cancel
                if key == 27:
                    cv2.destroyWindow(calib_window)
                    return

            self.color_calibration[requested_color] = img_ops.calc_hist_in_area(img, detection_poly)

    def run(self):
        stream, img = self.read_cam()

        while stream:
            stream, img = self.read_cam()
            self.process_img(img)

            if self.process_key_input():
                self.shutdown()
                break
