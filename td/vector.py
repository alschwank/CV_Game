import math
import numpy as np

def norm(vector):
    # based on http://stackoverflow.com/questions/2827393/angles-between-two-n-dimensional-vectors-in-python
    """ Returns the norm (length) of the vector."""
    # note: this is a very hot function, hence the odd optimization
    # Unoptimized it is: return np.sqrt(np.sum(np.square(vector)))
    return np.sqrt(np.dot(vector, vector))

def unit_vector(vector):
    # based on http://stackoverflow.com/questions/2827393/angles-between-two-n-dimensional-vectors-in-python
    """ Returns the unit vector of the vector.  """
    return vector / norm(vector)


class vec2d(object): # this says what to do with lists that have 2 values like (x, y)

    def __init__(self, x, y): # splits some (x, y) numbers into seperate numbers
        self.x = x
        self.y = y

    def __sub__(self, other): # subtract
        return vec2d(self.x - other.x, self.y - other.y)

    def __repr__(self): # print
        return "(%s, %s)" % (self.x, self.y)

    def __getitem__(self, key): # get one of the values from the vector
        return (self.x, self.y)[key]

    def get_length(self): # gets the length of a vector
        return math.sqrt((self.x ** 2 + self.y ** 2)) # equasion to get length

    def normalize(self): # this is dividing something by its length used for direction to move
        length = self.get_length() # gets the length
        if length != 0: # if we are not going to divide by zero
            return self.x / length, self.y / length # divides (x, y) by the length

        return self.x, self.y # if length == 0 then skip the division step
