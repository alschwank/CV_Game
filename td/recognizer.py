from __future__ import division
import cv2
import numpy as np
import pygame
from constants import CAMERA_PORT, CAM_RES_HEIGHT, CAM_RES_WIDTH, CAM_INVERTED, WINDOW_SIZE, FPS, GREEN_RGB, WIDTH, RED_RGB, RED_BGR, MIN_CORRELATION_COEFFICIENT
from input_process import Input_process
from image_operations import Image_operations

__author__ = 'Alexander'


class TowerRecognizer(Input_process):
    def __init__(self, difficulty=0):
        Input_process.__init__(self, create_windows=False, difficulty=difficulty)
        self.difficulty = difficulty
        self.init_camera()

    def init_camera(self):
        self.cam = cv2.VideoCapture(CAMERA_PORT)

        self.cam.set(3, CAM_RES_WIDTH)
        self.cam.set(4, CAM_RES_HEIGHT)

    def calibrate_pygame(self, screen, player):
        font = pygame.font.SysFont("sourcecodepro", 32)
        img_ops = Image_operations()

        # find middle
        stream = None
        while not stream:
            stream, img = self.read_cam()
            print stream
        x = int(img.shape[1] * 0.5)
        y = int(img.shape[0] * 0.5)
        radius = 20
        detection_poly = [(x + radius, y), (x, y + radius), (x - radius, y), (x, y - radius)]

        mask = np.zeros(img.shape, dtype="uint8")
        cv2.fillConvexPoly(mask, np.array([detection_poly], 'int32'), (1, 1, 1))
        clock = pygame.time.Clock()

        for requested_color in player.color_calibration.keys():
            waiting_for_user = True
            color_taken = False
            while waiting_for_user:
                clock.tick(FPS)
                stream, img = self.read_cam()
                img *= 0.5
                show_img = img * mask + img
                cv2.polylines(show_img, np.array([detection_poly], 'int32'), True, RED_BGR, thickness=1)
                show_img = cv2.resize(show_img, WINDOW_SIZE)
                screen.blit(pygame.surfarray.make_surface(np.rot90(show_img[:, :, ::-1])), (0, 0))

                text0 = font.render("Please define your color for the ", True, GREEN_RGB)
                text0_rect = text0.get_rect()
                text0_rect.center = (WIDTH / 2, 200)
                screen.blit(text0, text0_rect)

                text1 = font.render(requested_color, True, GREEN_RGB)
                text1_rect = text1.get_rect()
                text1_rect.center = (WIDTH / 2, 240)
                screen.blit(text1, text1_rect)

                if color_taken:
                    text2 = font.render("Color already taken.", True, RED_RGB)
                    text2_rect = text2.get_rect()
                    text2_rect.center = (WIDTH / 2, 540)
                    screen.blit(text2, text2_rect)

                pygame.display.update(text0_rect)
                pygame.display.update(text1_rect)

                for event in pygame.event.get():
                    if event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_RETURN:
                            player.color_calibration[requested_color] = img_ops.calc_hist_in_area(img, detection_poly)

                            #check if new color is already in calibrated colors
                            for other in player.color_calibration.keys():
                                if other != requested_color:
                                    h0 = player.color_calibration[requested_color]
                                    h1 = player.color_calibration[other]
                                    if h1 is None or img_ops.compare_hists(h0, h1) < MIN_CORRELATION_COEFFICIENT:
                                        waiting_for_user = False
                                    else:
                                        color_taken = True
                        if event.key == pygame.K_ESCAPE:
                            return -1
                    if event.type == pygame.QUIT:
                        return -1

                pygame.display.update()

        self.color_calibration = player.color_calibration
        return 0

    def run(self):
        stream, img = self.cam.read()
        combined_lines_img, indices, succ_status = self.process_shape_matching(img)

        # convert images to pygame format
        if CAM_INVERTED:
            combined_lines_img = cv2.flip(combined_lines_img, flipCode=0)
        combined_lines_img = cv2.resize(combined_lines_img, (430, 300))

        return combined_lines_img, indices, succ_status