import pygame
import cv2

from stats import *
from constants import *
from wave_stats import *


class Shapes_Display(pygame.sprite.Sprite):
    def __init__(self, shape_collection):
        pygame.sprite.Sprite.__init__(self)
        self.shape_collection = shape_collection
        self.image = pygame.Surface((WIDTH, TOWER_SIZE))
        self.image.fill((125, 125, 125)) 
        self.rect = self.image.get_rect()
        self.rect.topleft = (0, 600)

    def update(self):
        # convert BGR to RGB and create pygame surface
        self.image = pygame.surfarray.make_surface(cv2.resize(cv2.flip(self.shape_collection.get_combined_img(rotate=True)[:,:,::-1], 0), (213, 850) ))

class Build_Tower_Bar(pygame.sprite.Sprite):

    def __init__(self):
        self.groups = huds
        pygame.sprite.Sprite.__init__(self, self.groups)

        earth = Build_Tower_Button(EARTH)
        fire = Build_Tower_Button(FIRE)
        water = Build_Tower_Button(WATER)
        upgrade = Upgrade_Tower_Button()
        self.buttons =  [earth, fire, water, upgrade]
        self.image = pygame.Surface((0, 0))
        self.image.fill((0, 0, 0))
        self.rect = self.image.get_rect()
        self.rect.topleft = (0, 0)

    def update(self, player):
        for button in self.buttons:
            button.update(player)

class Manual_Shot_Button(pygame.sprite.Sprite):

    def __init__(self):
        self.groups = huds
        pygame.sprite.Sprite.__init__(self, self.groups)

        self.is_active = False
        self.tower_stats = Tower_Stats(MANUAL_SHOT)

        self.active_bg = pygame.Surface((130, 50))
        self.active_bg.fill(GREEN_RGB)
        self.active_bg_mouse_over = self.active_bg.copy()

        self.inactive_bg = pygame.Surface((130, 50))
        self.inactive_bg.fill(GRAY)
        self.inactive_bg_mouse_over = self.inactive_bg.copy()

        self.image = self.inactive_bg

        self.rect = self.active_bg.get_rect()
        self.rect.center = HUDS_MARGIN[0], HUDS_MARGIN[1] + 400

        self.font = pygame.font.Font('Resources/Fonts/Microsoft Sans Serif.ttf', 14)

        self.picture = self.tower_stats['image']

        # normal text
        self.normal_text = self.font.render("Manual shot", 1, (255, 255, 255))

        self.active_bg.blit(self.normal_text, (45,16))
        self.active_bg.blit(self.picture, (((self.image.get_height() - self.picture.get_height()) / 2),
                                        ((self.image.get_height() - self.picture.get_height()) / 2)))
        self.inactive_bg.blit(self.normal_text, (45,16))
        self.inactive_bg.blit(self.picture, (((self.image.get_height() - self.picture.get_height()) / 2),
                                        ((self.image.get_height() - self.picture.get_height()) / 2)))

        # text when mouse is over button
        self.tower_range = self.tower_stats['radius']
        self.tower_range_text = 'Range: %s' % self.tower_range
        self.tower_range_surface = self.font.render(self.tower_range_text, 1, (0,0,0))
        self.inactive_bg_mouse_over.blit(self.tower_range_surface, (1,1))
        self.active_bg_mouse_over.blit(self.tower_range_surface, (1,1))

        self.tower_attack_rate = self.tower_stats['speed']
        self.tower_attack_rate_text = 'Rate: %s' % self.tower_attack_rate
        self.tower_attack_rate_surface = self.font.render(self.tower_attack_rate_text, 1, (0,0,0))
        self.inactive_bg_mouse_over.blit(self.tower_attack_rate_surface, (1,16))
        self.active_bg_mouse_over.blit(self.tower_attack_rate_surface, (1,16))

        self.tower_attack_damage = self.tower_stats['bullet_damage']
        self.tower_damage_text = 'Damage: %s' % self.tower_attack_damage
        self.tower_damage_surface = self.font.render(self.tower_damage_text, 1, (0,0,0))
        self.inactive_bg_mouse_over.blit(self.tower_damage_surface, (1,31))
        self.active_bg_mouse_over.blit(self.tower_damage_surface, (1,31))

    def update(self, player):
        mouse = pygame.mouse.get_pos()
        if self.is_active:
            if self.rect.collidepoint(mouse):
                self.image = self.active_bg_mouse_over
            else:
                self.image = self.active_bg
        else:
            if self.rect.collidepoint(mouse):
                self.image = self.inactive_bg_mouse_over
            else:
                self.image = self.inactive_bg

class Build_Tower_Button(pygame.sprite.Sprite):

    def __init__(self, tower_type):
        self.groups = huds
        pygame.sprite.Sprite.__init__(self, self.groups)

        ##### tower stats #####
        self.tower_stats = Tower_Stats(tower_type)
        self.type = tower_type

        self.is_active = False

        self.active_bg = pygame.Surface((200, 50))
        self.active_bg.fill(GREEN_RGB)
        self.active_bg.set_alpha(200)
        self.active_bg_mouse_over = self.active_bg.copy()

        self.inactive_bg = pygame.Surface((200, 50))
        self.inactive_bg.fill(GRAY) 
        self.inactive_bg.set_alpha(200)
        self.inactive_bg_mouse_over = self.inactive_bg.copy()

        self.image = self.inactive_bg

        self.rect = self.active_bg.get_rect()
        self.rect.topleft = self.tower_stats['rect_topleft']

        self.font = pygame.font.Font('Resources/Fonts/Microsoft Sans Serif.ttf', 14)

        self.picture = self.tower_stats['image']

        # normal text
        self.normal_text = self.font.render(tower_type['name'], 1, (255, 255, 255))

        self.active_bg.blit(self.normal_text, (60,16))
        self.active_bg.blit(self.picture, (((self.image.get_height() - self.picture.get_height()) / 2),
                                        ((self.image.get_height() - self.picture.get_height()) / 2)))
        self.inactive_bg.blit(self.normal_text, (60,16))
        self.inactive_bg.blit(self.picture, (((self.image.get_height() - self.picture.get_height()) / 2),
                                        ((self.image.get_height() - self.picture.get_height()) / 2)))

        # text when mouse is over button
        self.tower_range = self.tower_stats['radius']
        self.tower_range_text = 'Range: %s' % self.tower_range
        self.tower_range_surface = self.font.render(self.tower_range_text, 1, (0,0,0))
        self.inactive_bg_mouse_over.blit(self.tower_range_surface, (1,1))
        self.active_bg_mouse_over.blit(self.tower_range_surface, (1,1))

        self.tower_attack_rate = self.tower_stats['speed']
        self.tower_attack_rate_text = 'Rate: %s' % self.tower_attack_rate
        self.tower_attack_rate_surface = self.font.render(self.tower_attack_rate_text, 1, (0,0,0))
        self.inactive_bg_mouse_over.blit(self.tower_attack_rate_surface, (1,16))
        self.active_bg_mouse_over.blit(self.tower_attack_rate_surface, (1,16))

        self.tower_attack_damage = self.tower_stats['bullet_damage']
        self.tower_damage_text = 'Damage: %s' % self.tower_attack_damage
        self.tower_damage_surface = self.font.render(self.tower_damage_text, 1, (0,0,0))
        self.inactive_bg_mouse_over.blit(self.tower_damage_surface, (1,31))
        self.active_bg_mouse_over.blit(self.tower_damage_surface, (1,31))


    def update(self, player):
        mouse = pygame.mouse.get_pos()
        if self.is_active:
            if self.rect.collidepoint(mouse):
                self.image = self.active_bg_mouse_over
            else:
                self.image = self.active_bg
        else:
            if self.rect.collidepoint(mouse):
                self.image = self.inactive_bg_mouse_over
            else:
                self.image = self.inactive_bg

class Upgrade_Tower_Button(pygame.sprite.Sprite):

    def __init__(self):
        self.groups = huds
        pygame.sprite.Sprite.__init__(self, self.groups)

        self.stats = Upgrade_Stats()
        self.type = UPGRADE

        self.is_active = False

        self.active_bg = pygame.Surface((200, 50)) 
        self.active_bg.fill(WHITE) 
        self.active_bg.set_alpha(200)
        self.active_bg_mouse_over = self.active_bg.copy()

        self.inactive_bg = pygame.Surface((200, 50)) 
        self.inactive_bg.fill(GRAY) 
        self.inactive_bg.set_alpha(200)
        self.inactive_bg_mouse_over = self.inactive_bg.copy()

        self.image = self.inactive_bg

        self.rect = self.active_bg.get_rect()
        self.rect.topleft = self.stats.rect_topleft

        self.font = pygame.font.Font('Resources/Fonts/Microsoft Sans Serif.ttf', 14)

        self.picture = pygame.image.load(os.path.join('Resources','Images','Sprites','Towers','upgrade.png')).convert_alpha()

        # normal text
        self.tower_type_text = "Tower Upgrade"
        self.normal_text = self.font.render(self.tower_type_text, 1, BLACK)

        self.active_bg.blit(self.normal_text, (60,16))
        self.active_bg.blit(self.picture, (((self.image.get_height() - self.picture.get_height()) / 2),
                                        ((self.image.get_height() - self.picture.get_height()) / 2)))
        self.inactive_bg.blit(self.normal_text, (60,16))
        self.inactive_bg.blit(self.picture, (((self.image.get_height() - self.picture.get_height()) / 2),
                                        ((self.image.get_height() - self.picture.get_height()) / 2)))

        # text when mouse is over button
        self.tower_range = self.stats.range
        self.tower_range_text = "Range: +%s %%" % ((self.tower_range - 1) * 100)
        self.tower_range_surface = self.font.render(self.tower_range_text, 1, (0,0,0))
        self.inactive_bg_mouse_over.blit(self.tower_range_surface, (1,1))
        self.active_bg_mouse_over.blit(self.tower_range_surface, (1,1))

        self.tower_attack_rate = self.stats.speed
        self.tower_attack_rate_text = 'Rate: +%s %%' % ((self.tower_attack_rate - 1) * 100)
        self.tower_attack_rate_surface = self.font.render(self.tower_attack_rate_text, 1, (0,0,0))
        self.inactive_bg_mouse_over.blit(self.tower_attack_rate_surface, (1,16))
        self.active_bg_mouse_over.blit(self.tower_attack_rate_surface, (1,16))

        self.tower_attack_damage = self.stats.damage
        self.tower_damage_text = 'Damage: +%s %%' % ((self.tower_attack_damage - 1) * 100)
        self.tower_damage_surface = self.font.render(self.tower_damage_text, 1, (0,0,0))
        self.inactive_bg_mouse_over.blit(self.tower_damage_surface, (1,31))
        self.active_bg_mouse_over.blit(self.tower_damage_surface, (1,31))


    def update(self, player):
        mouse = pygame.mouse.get_pos()
        if self.is_active:
            if self.rect.collidepoint(mouse):
                self.image = self.active_bg_mouse_over
            else:
                self.image = self.active_bg
        else:
            if self.rect.collidepoint(mouse):
                self.image = self.inactive_bg_mouse_over
            else:
                self.image = self.inactive_bg

class Background(pygame.sprite.Sprite):

    def __init__(self):
        self.groups = backgrounds
        pygame.sprite.Sprite.__init__(self, self.groups)
        background = pygame.image.load(os.path.join('Resources','Images','Menu Images','In Game Menu','background.png')).convert()
        self.image = background
        self.rect = self.image.get_rect()
        self.rect.topleft = (0,0)

class Detection_Info(pygame.sprite.Sprite):

    def __init__(self):
        self.groups = detection_info_group
        pygame.sprite.Sprite.__init__(self, self.groups)

        self.image = pygame.Surface((130, 100)) 
        self.image.fill(GRAY) 
        self.rect = self.image.get_rect()
        self.rect.center = HUDS_MARGIN[0], HUDS_MARGIN[1] + 300

        self.font = pygame.font.Font('Resources/Fonts/Microsoft Sans Serif.ttf', 14)

        texts = ['Found Shape', 'Edge Count', 'Angles', 'Edge Length', 'Triangle Color', 'Rest Color']
        for i, text in enumerate(texts):
            surface = self.font.render(text, 1, (0,0,0))
            self.image.blit(surface, (1, 15 * i + 7))


    def update(self, succ_status):
        if succ_status == DETECTION_NONE:
            for i in range(0, DETECTION_STATUS_COUNT):
                pygame.draw.circle(self.image, RED_RGB, (110, 15 * (i+1)), 6)
        else:
            for i in range(0,succ_status + 1):
                pygame.draw.circle(self.image, GREEN_RGB, (110, 15 * (i+1)), 6)

            for i in range(succ_status + 1, DETECTION_STATUS_COUNT):
                pygame.draw.circle(self.image, RED_RGB, (110, 15 * (i+1)), 6)


class Info_PopUp(pygame.sprite.Sprite):  # box at bottom of screen that displays useful info

    def __init__(self):
        self.groups = huds
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.size = (550,80)

        self.image = pygame.Surface(self.size) 
        self.image.fill((0, 0, 0)) 
        self.rect = self.image.get_rect()
        self.rect.bottomleft = (MAP_MARGIN[0] + 150, MAP_MARGIN[1] + 590)
        self.alpha_value = 0
        self.fade_out_speed = 6
        self.time_till_fade_out = 0
        self.font = pygame.font.Font(os.path.join('Resources','Fonts','Microsoft Sans Serif.ttf'), 14)
        self.font_color = (0,0,0)

        self.last_temp_info_type = None  #stores the most recent info type that was temporarily displayed on the info button
        self.displaying_message = False  # we are not displaying anything

    def display_info(self, info, delayed_fade_out):

        self.alpha_value = 180
        if delayed_fade_out:
            self.time_till_fade_out = 60
        else:
            self.time_till_fade_out = 0

        if not self.displaying_message:  # if nothing is already displaying, then we can display something new
            self.displaying_message = True
            self.temp_image = pygame.Surface(self.size) 
            self.temp_image.fill((255, 255, 255)) 

            self.temp_message_text = info
            self.temp_message_surface = self.font.render(self.temp_message_text, 1, self.font_color)
            self.temp_image.blit(self.temp_message_surface, (((self.size[0] - self.temp_message_surface.get_width())/2),40))
            self.last_temp_info_type = info
            self.image = self.temp_image


    def fade_out(self):
        if self.time_till_fade_out <= 0:
            if self.alpha_value >= 0:
                self.alpha_value -= self.fade_out_speed
            else:
                self.displaying_message = False
        else:
            self.time_till_fade_out -= 1

    def update(self, player):
        self.image.set_alpha(self.alpha_value)

class Direction_Arrow(pygame.sprite.Sprite):

    def __init__(self):
        self.groups = huds
        pygame.sprite.Sprite.__init__(self, self.groups)
        orig_image = pygame.image.load(os.path.join('Resources','Images','Menu Images','In Game Menu','arrow.png')).convert()
        self.images = { RIGHT : orig_image,
                        UP : pygame.transform.rotate(orig_image, 90),
                        LEFT : pygame.transform.rotate(orig_image, 180),
                        DOWN : pygame.transform.rotate(orig_image, 270)}

        self.alpha_value = 0
        self.max_alpha = 255
        self.min_alpha = 0
        self.alpha_speed = 5
        self.adding_more_alpha = False
        self.vanishing = False

    def start_new_map(self, player):
        direction = player.game_map.dirs[0]
        self.image = self.images[direction]
        self.image.set_colorkey(self.image.get_at((0, 0))) # transparent
        self.rect = self.image.get_rect()
        if direction == RIGHT:
            self.rect.center = (MAP_MARGIN[0] + (player.game_map.start[0] + 1)  * TILE_SIZE + TILE_SIZE/2,
                                MAP_MARGIN[1] + (player.game_map.start[1] - 1) * TILE_SIZE + TILE_SIZE/2)
        else:
            self.rect.center = (MAP_MARGIN[0] + player.game_map.start[0] * TILE_SIZE + TILE_SIZE/2,
                                MAP_MARGIN[1] + player.game_map.start[1] * TILE_SIZE + TILE_SIZE/2)

    def vanish(self):
        self.vanishing = True

    def update(self, player):
        if not self.vanishing:
            if self.adding_more_alpha:
                self.alpha_value += self.alpha_speed
                if self.alpha_value >= self.max_alpha:
                    self.adding_more_alpha = False
            else:
                self.alpha_value -= self.alpha_speed
                if self.alpha_value <= self.min_alpha:
                    self.adding_more_alpha = True
        else:
            if self.alpha_value >= self.min_alpha:
                self.alpha_value -= self.alpha_speed

        self.image.set_alpha(self.alpha_value)

class Speed_Toggle(pygame.sprite.Sprite):

    def __init__(self):
        self.groups = huds
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.font = pygame.font.Font(os.path.join('Resources','Fonts','Microsoft Sans Serif.ttf'), 14)
        self.image_slow = self.font.render('SLOW DOWN <<', 1, (25, 25, 255))
        self.image_speed = self.font.render('SPEED UP >>', 1, (255, 25, 25))
        self.image = self.image_speed
        self.rect = self.image.get_rect()
        self.rect.center = HUDS_MARGIN[0], HUDS_MARGIN[1] + 500
        self.activated_speed = False

    def change_setting(self):
        self.activated_speed = not self.activated_speed

    def update(self, player):
        if self.activated_speed:
            self.image = self.image_slow
        else:
            self.image = self.image_speed


class Sound_Toggle(pygame.sprite.Sprite):

    def __init__(self):
        self.groups = huds
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.font = pygame.font.Font(os.path.join('Resources','Fonts','Microsoft Sans Serif.ttf'), 14)
        self.sound_setting = 'ON/OFF'
        self.text = 'SOUND ' + self.sound_setting
        self.image1 = self.font.render(self.text, 1, (255, 255, 255))
        self.image2 = self.font.render(self.text, 1, (255, 25, 25))
        self.image = self.image1
        self.rect = self.image.get_rect()
        self.rect.center = HUDS_MARGIN[0], HUDS_MARGIN[1] + 480
        self.sound_on = True

    def change_setting(self, music_on):
        if music_on:
            self.sound_on = True
            self.sound_setting = 'ON'
        elif not music_on:
            self.sound_on = False
            self.sound_setting = 'OFF'
        self.text = 'SOUND ' + self.sound_setting
        self.image1 = self.font.render(self.text, 1, (255, 255, 255))
        self.image2 = self.font.render(self.text, 1, (255, 25, 25))
        self.image = self.image1
        self.rect = self.image.get_rect()
        self.rect.center = HUDS_MARGIN[0], HUDS_MARGIN[1] + 480


    def update(self, player):
        self.image = self.image1
        mouse = pygame.mouse.get_pos()
        if self.rect.collidepoint(mouse):
            self.image = self.image2
            self.rect = self.image.get_rect()
            self.rect.center = HUDS_MARGIN[0], HUDS_MARGIN[1] + 480


class Pause(pygame.sprite.Sprite):

    def __init__(self):
        self.groups = huds
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.font = pygame.font.Font(os.path.join('Resources','Fonts','Microsoft Sans Serif.ttf'), 14)
        self.text = 'PAUSE GAME'
        self.image1 = self.font.render(self.text, 1, (255, 255, 255))
        self.image2 = self.font.render(self.text, 1, (255, 25, 25))
        self.image = self.image1
        self.rect = self.image.get_rect()
        self.rect.center = HUDS_MARGIN[0], HUDS_MARGIN[1] + 520

    def update(self, player):
        self.image = self.image1
        mouse = pygame.mouse.get_pos()
        if self.rect.collidepoint(mouse):
            self.image = self.image2
            self.rect = self.image.get_rect()
            self.rect.center = HUDS_MARGIN[0], HUDS_MARGIN[1] + 520

class Main_Menu(pygame.sprite.Sprite):

    def __init__(self):
        self.groups = huds
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.font = pygame.font.Font(os.path.join('Resources','Fonts','Microsoft Sans Serif.ttf'), 14)
        self.text = 'MAIN MENU'
        self.image1 = self.font.render(self.text, 1, (255, 255, 255))
        self.image2 = self.font.render(self.text, 1, (255, 25, 25))
        self.image = self.image1
        self.rect = self.image.get_rect()
        self.rect.center = HUDS_MARGIN[0], HUDS_MARGIN[1] + 540

    def update(self, player):
        self.image = self.image1
        mouse = pygame.mouse.get_pos()
        if self.rect.collidepoint(mouse):
            self.image = self.image2
            self.rect = self.image.get_rect()
            self.rect.center = HUDS_MARGIN[0], HUDS_MARGIN[1] + 540

class Next_Wave(pygame.sprite.Sprite):

    def __init__(self):
        self.groups = huds
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.font = pygame.font.Font(os.path.join('Resources','Fonts','Microsoft Sans Serif.ttf'), 14)
        self.text = 'NEXT WAVE'
        self.image1 = self.font.render(self.text, 1, (255, 255, 255))
        self.image2 = self.font.render(self.text, 1, (255, 25, 25))
        self.image = self.image1
        self.rect = self.image.get_rect()
        self.rect.center = HUDS_MARGIN[0], HUDS_MARGIN[1] + 35

    def update(self, player):
        self.image = self.image1
        mouse = pygame.mouse.get_pos()
        if self.rect.collidepoint(mouse):
            self.image = self.image2
            self.rect = self.image.get_rect()
            self.rect.center = HUDS_MARGIN[0], HUDS_MARGIN[1] + 35

class Life(pygame.sprite.Sprite):

    def __init__(self, player):
        self.groups = huds
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.font = pygame.font.Font(os.path.join('Resources','Fonts','Microsoft Sans Serif.ttf'), 14)
        self.life = player.life
        self.text = 'LIFE: %s' % self.life
        self.image = self.font.render(self.text, 1, (255, 255, 255))
        self.rect = self.image.get_rect()
        self.rect.center = HUDS_MARGIN[0], HUDS_MARGIN[1] + 15

    def update(self, player):
        self.life = player.life
        self.text = 'LIFE: %s' % self.life
        self.image = self.font.render(self.text, 1, (255, 255, 255))

class Wave_Info(pygame.sprite.Sprite):

    def __init__(self, wave):
        self.groups = huds, wave_info_group
        pygame.sprite.Sprite.__init__(self, self.groups)

        self.font = pygame.font.Font(os.path.join('Resources','Fonts','Microsoft Sans Serif.ttf'), 14)
        self.wave = wave.current_wave

        self.image = pygame.Surface((130, 50)) 
        self.image.fill((0, 100, 100)) 
        self.rect = self.image.get_rect()
        self.rect.center = HUDS_MARGIN[0], HUDS_MARGIN[1] + 100

        self.wave = wave.current_wave
        self.ammount_text = 'WAVE:%s' % self.wave + '/%s' % (len(wave.map_enemies))
        self.ammount_surface = self.font.render(self.ammount_text, 1, (255,255,255))
        self.image.blit(self.ammount_surface, (1,1))

        self.wave_kind_text = '%s' % wave.wave_type
        self.wave_kind_surface = self.font.render(self.wave_kind_text, 1, (255,255,255))
        self.image.blit(self.wave_kind_surface, (1,16))

        self.wave_enemy_life_base = enemy_stats(wave.wave_type)['base_life']
        self.wave_enemy_life_text = 'Life:%s' % (self.wave_enemy_life_base * wave.life_multiplier) # calculate enemy health
        self.wave_enemy_life_surface = self.font.render(self.wave_enemy_life_text, 1, (255,255,255))
        self.image.blit(self.wave_enemy_life_surface, (1,31))

class Time_to_next_Wave(pygame.sprite.Sprite):

    def __init__(self, time):
        self.groups = huds
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.font = pygame.font.Font(os.path.join('Resources','Fonts','Microsoft Sans Serif.ttf'), 14)
        self.text = "Next wave in: "
        self.image = self.font.render(self.text + str(time), 1, (255, 255, 255))
        self.rect = self.image.get_rect()
        self.rect.center = HUDS_MARGIN[0], HUDS_MARGIN[1] + 50

    def update(self, time):
        if time >= 0:
            self.image = self.font.render(self.text + str(time), 1, (255, 255, 255))
        else:
            self.show_wave_is_coming()

    def show_wave_is_coming(self):
        self.image = self.font.render("Wave is coming.", 1, (255, 255, 255))

class Info_Bar_Background(pygame.sprite.Sprite):  # create a box that menu buttons can be posted ontop of

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((140, 280))
        self.image.fill(GRAY) 
        self.image.set_alpha(100) # make the image see through 255 is most visible
        self.rect = self.image.get_rect()
        self.rect.center = HUDS_MARGIN[0], HUDS_MARGIN[1] + 210


class Button(pygame.sprite.Sprite):

    def __init__(self, slot, button_type, tower, tower_type):
        self.groups = huds, buttons
        pygame.sprite.Sprite.__init__(self, self.groups)

        self.button_type = button_type
        self.button_y_start = 160

        self.width = 140
        self.height = 50
        self.image = pygame.Surface((self.width, self.height)) 
        self.image.fill((100, 255, 100)) 

        self.rect = self.image.get_rect()
        self.rect.center = (HUDS_MARGIN[0], HUDS_MARGIN[1] + ((slot * 55) + self.button_y_start))
        self.font = pygame.font.Font(os.path.join('Resources','Fonts','Microsoft Sans Serif.ttf'), 14)

        if button_type == 'tower info': # display tower info
            if tower is None:
                self.info_stats = Tower_Stats(tower_type)
                self.tower_range = self.info_stats['radius']
                self.tower_attack_rate = self.info_stats['speed']
                self.tower_attack_damage = self.info_stats['bullet_damage']
            else:
                self.tower_range = tower.range
                self.tower_attack_rate = tower.speed
                self.tower_attack_damage = tower.damage

            self.tower_range_text = 'Range: %.2f' % self.tower_range
            self.tower_range_surface = self.font.render(self.tower_range_text, 1, (0,0,0))
            self.image.blit(self.tower_range_surface, (1,1))

            self.tower_attack_rate_text = 'Rate: %.2f' % self.tower_attack_rate
            self.tower_attack_rate_surface = self.font.render(self.tower_attack_rate_text, 1, (0,0,0))
            self.image.blit(self.tower_attack_rate_surface, (1,16))

            self.tower_damage_text = 'Damage: %.2f' % self.tower_attack_damage
            self.tower_damage_surface = self.font.render(self.tower_damage_text, 1, (0,0,0))
            self.image.blit(self.tower_damage_surface, (1,31))

        if button_type == 'upgrade info': # display tower info
            self.upgrade_stats = Upgrade_Stats()

            self.tower_range_text = 'Range: +%s %%' % ((self.upgrade_stats.range - 1) * 100)
            self.tower_range_surface = self.font.render(self.tower_range_text, 1, (0,0,0))
            self.image.blit(self.tower_range_surface, (1,1))

            self.tower_attack_rate_text = 'Rate: +%s %%' % ((self.upgrade_stats.speed - 1) * 100)
            self.tower_attack_rate_surface = self.font.render(self.tower_attack_rate_text, 1, (0,0,0))
            self.image.blit(self.tower_attack_rate_surface, (1,16))

            self.tower_damage_text = 'Damage: +%s %%' % ((self.upgrade_stats.damage - 1) * 100)
            self.tower_damage_surface = self.font.render(self.tower_damage_text, 1, (0,0,0))
            self.image.blit(self.tower_damage_surface, (1,31))

        if button_type == 'cancel placement' \
            or button_type == 'cancel upgrading'\
            or button_type == 'cancel shot':
            self.image.fill(RED_RGB) 
            self.info_text = 'To ' + button_type
            self.info_surface = self.font.render(self.info_text, 1, (255, 255, 255))

            self.hotkey_text = '[Space/Right Click]'
            self.hotkey_surface = self.font.render(self.hotkey_text, 1, (255, 255, 255))

            self.image.blit(self.info_surface, (((self.width - self.info_surface.get_width())/2),1))
            self.image.blit(self.hotkey_surface, (((self.width - self.hotkey_surface.get_width())/2),16))