from wave_stats import *
from music import *
from constants import LIFE_MULTIPLIER


def reset(player, wave):
    """
    Function:
        Clears all the game elements and resets everything so
        we can start the game over
    """
    player.life = player.LIFE # reset life
    player.game_over = False

    music.reset()
    wave.creating_wave = False # we are not creating a wave anymore
    wave.wave_done = True
    wave.last_wave = False
    wave.wave_ammount = None # reset number of enemies that are waiting to come
    wave.wave_type = None
    wave.map_enemies = None # holds enemies for which map we are currently on
    wave.current_wave = 0
    wave.life_multiplier = LIFE_MULTIPLIER

    for o in all_objects:
        o.kill() # destroy every sprite thats in the all group  
