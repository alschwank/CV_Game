from unittest import TestCase

import numpy as np

from td.shape_generation import PolygonGenerationAlgorithm


__author__ = 'Alexander'


class TestPolygonGenerationAlgorithm(TestCase):
    def test_angle_between(self):
        algo = PolygonGenerationAlgorithm()
        np.array([0, 0])
        angle = algo.angle_between([np.array([0, 0]), np.array([0, 1])], [np.array([0, 0]), np.array([1, 0])])
        self.assertEqual(angle, 90)

        angle = algo.angle_between([np.array([0, 0]), np.array([1, 0])], [np.array([0, 0]), np.array([0, 1])])
        self.assertEqual(angle, 90)

        angle = algo.angle_between([np.array([0, 0]), np.array([0, 1])], [np.array([0, 0]), np.array([0, -1])])
        self.assertEqual(angle, 180)

        angle = algo.angle_between([np.array([0, 0]), np.array([0, 1])], [np.array([0, 0]), np.array([0, 1])])
        self.assertEqual(angle, 0)

        angle = algo.angle_between([np.array([0, 0]), np.array([0, 1])], [np.array([0, 0]), np.array([1, 1])])
        self.assertTrue(abs(angle - 45) < 0.1)

    def test_is_valid(self):
        algo = PolygonGenerationAlgorithm()
        points = [np.array([81, 238]), np.array([118, 207]), np.array([85, 101]), np.array([109, 92]),
                  np.array([218, 131]), np.array([81, 238])]
        is_valid, angles = algo.is_valid(points)
        self.assertFalse(is_valid)

    def test_intersects(self):
        algo = PolygonGenerationAlgorithm()
        found_intersection = False
        points = [np.array([0, 202]), np.array([122, 120]), np.array([68, 22]), np.array([21, 46]),
                  np.array([140, 238]), np.array([0, 202])]

        line0 = [points[0], points[1]]
        line1 = [points[3], points[4]]
        self.assertTrue(algo.intersects(line0, line1))

        for i in range(0, len(points) - 2):
            # check if there's an intersection
            line0 = [points[i], points[i + 1]]
            for k in range(i + 2, len(points) - 1):
                line1 = [points[k], points[k + 1]]
                if (line0[0][0] != line1[1][0] or line0[0][1] != line1[1][1]) \
                    and (line0[1][0] != line1[0][0] or line0[1][1] != line1[0][1]):
                    intersects = algo.intersects(line0, line1)
                    if intersects:
                        found_intersection = True
                        self.assertEqual(i, 0)
                        self.assertEqual(k, 3)

        self.assertTrue(found_intersection)