from unittest import TestCase

import numpy as np

from input_process import Input_process
from td.vector_match import Line
from td.shape import Shape


__author__ = 'Alexander'


class TestInput_process(TestCase):
    def test_angle_between(self):
        ip = Input_process()

        a = Line(np_array=np.array([0, 0, 0, 1]))
        b = Line(np_array=np.array([0, 0, 1, 0]))
        angle = ip.angle_between(a, b)
        self.assertEqual(angle, 90)

        a = Line(np_array=np.array([0, 0, 1, 0]))
        b = Line(np_array=np.array([0, 0, 0, 1]))
        angle = ip.angle_between(a, b)
        self.assertEqual(angle, 90)

        a = Line(np_array=np.array([0, 0, 1, 0]))
        b = Line(np_array=np.array([0, 0, 1, 1]))
        angle = ip.angle_between(a, b)
        self.assertAlmostEqual(angle, 45, places=1)

    def test_calc_angles(self):
        ip = Input_process()

        a0 = Line(np_array=np.array([0, 0, 10, 0]))
        a1 = Line(np_array=np.array([10, 0, 10, 10]))
        a2 = Line(np_array=np.array([10, 10, 0, 10]))
        a3 = Line(np_array=np.array([0, 10, 0, 0]))
        lines = [a0, a1, a2, a3]

        angles = ip.calc_angles(lines)
        self.assertListEqual(angles, [90, 90, 90, 90])

        a0 = Line(np_array=np.array([0, 0, 10, 0]))
        a1 = Line(np_array=np.array([10, 0, 10, 10]))
        a2 = Line(np_array=np.array([10, 10, 0, 10]))
        a3 = Line(np_array=np.array([0, 10, 0, 0]))
        lines = [a0, a1, a2, a3]

        angles = ip.calc_angles(lines)
        self.assertListEqual(angles, [90, 90, 90, 90])

        a0 = Line(np_array=np.array([500, 237, 450, 257]))
        a1 = Line(np_array=np.array([450, 257, 401, 356]))
        a2 = Line(np_array=np.array([401, 356, 402, 369]))
        a3 = Line(np_array=np.array([402, 369, 439, 391]))
        a4 = Line(np_array=np.array([439, 391, 532, 367]))
        a5 = Line(np_array=np.array([532, 367, 504, 237]))
        a6 = Line(np_array=np.array([504, 237, 500, 237]))

        lines = [a0, a1, a2, a3, a4, a5, a6]
        angles = ip.calc_angles(lines)
        self.assertListEqual(angles, [41.865504787868396, 30.731791080775317, 54.865806943084372, 45.205781801985992,
                                      87.684647597156356, 77.845058302777772, 21.801409486351812])

        points = [np.array([0, 50]), np.array([199, 15]), np.array([238, 32]), np.array([147, 217]),
                  np.array([68, 232]), np.array([0, 50])]
        s = Shape()
        s.generate_by_points(points)

        expected = [33.527, 92.64, 53.067, 80.264, 100.512]
        for i in range(0, len(points) - 1):
            self.assertAlmostEqual(s.angles[i], expected[i], places=1)

    def test_compare_angles(self):
        ip = Input_process()

        a = [41.865504787868396, 30.731791080775317, 54.865806943084372, 45.205781801985992, 87.684647597156356,
             77.845058302777772, 21.801409486351812]
        self.assertTrue(ip.compare_angles(a, a))

        a = [41.865504787868396, 30.731791080775317, 54.865806943084372, 45.205781801985992, 87.684647597156356,
             77.845058302777772, 21.801409486351812]
        b = [30.731791080775317, 54.865806943084372, 45.205781801985992, 87.684647597156356, 77.845058302777772,
             21.801409486351812, 41.865504787868396]
        self.assertTrue(ip.compare_angles(a, b))

        a = [41.865504787868396, 30.731791080775317, 54.865806943084372, 45.205781801985992, 87.684647597156356,
             77.845058302777772, 21.801409486351812]
        b = [30, 54, 45, 87, 77, 21, 41]
        self.assertTrue(ip.compare_angles(a, b))

        a = [41.865504787868396, 30.731791080775317, 54.865806943084372, 45.205781801985992, 87.684647597156356,
             77.845058302777772, 21.801409486351812]
        b = [30, 0, 45, 3, 77, 21, 41]
        self.assertFalse(ip.compare_angles(a, b))