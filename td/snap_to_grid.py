from constants import TILE_SIZE


def snap_to_grid(position):
    """
    recieves a position and changes it so that the position is
    centered to the nearest tile
    """
    x = position[0] # get value
    x //= TILE_SIZE # break down into ammount of tiles two division signs rounds to the nearest integer
    x *= TILE_SIZE # multiply tiles into pixels again
    x += (TILE_SIZE / 2) # add value to reach the center of the tile

    y = position[1]
    y //= TILE_SIZE
    y *= TILE_SIZE
    y += (TILE_SIZE / 2)

    return x, y
