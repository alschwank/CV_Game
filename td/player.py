class Player(): # all the info for the player
    def __init__(self):
        self.LIFE = 10 # player's life
        self.life = self.LIFE # so we can reset the life when the game is over
        self.map = None # the map that we are playing on
        self.game_map = None
        self.game_over = False
        self.game_won = False
        self.quiting = False
        o = True # open map
        c = False # closed map
        self.open_maps = [o, c, c, c, c, c]
        self.color_calibration = {"background": None, "color_triangle": None}
        self.activated_speed = False

    def open_new_map(self):
        self.open_maps[self.map + 1] = True
