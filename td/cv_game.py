import cam_process

__author__ = 'Alexander'

if __name__ == "__main__":
    print "Starting program."
    test = cam_process.Cam_Process()

    print "Running..."
    test.calibrate()
    test.run()
    print "Closing program."
