import os
from unittest import TestCase
from td.constants import DOWN, RIGHT, LEFT, UP
from td.tiledtmxloader import TileMapParser, Map_Directions_Detector

__author__ = 'Alexander'


class TestMap_Directions_Detector(TestCase):
    def load_layer(self, map_id):

        self.world_map = TileMapParser().parse_decode(
            os.path.join('Resources', 'Map Data', 'map' + str(map_id) + '.tmx'))
        for layer in self.world_map.layers[:]:
            if layer.name == 'collision':
                return layer
        return None

    def test_run(self):
        layer = self.load_layer(2)
        detector = Map_Directions_Detector()
        result_dirs, result_lengths, result_start = detector.run(layer)
        expected_dirs = [RIGHT, UP, LEFT, UP, RIGHT, DOWN]
        expected_lengths = [7, 3, 4, 3, 7, 10]
        expected_start = (0, 8)
        self.assertListEqual(expected_dirs, result_dirs)
        self.assertListEqual(result_lengths, expected_lengths)
        self.assertEqual(expected_start, result_start)

    def test_find_next_dir(self):
        layer = self.load_layer(0)
        detector = Map_Directions_Detector()
        start, direction = (11, 0), DOWN
        next_pos, next_dir = detector.find_next_dir(layer, start, direction)
        self.assertEqual(next_pos, (11, 1))
        self.assertEqual(next_dir, DOWN)

        start, direction = (11, 2), DOWN
        next_pos, next_dir = detector.find_next_dir(layer, start, direction)
        self.assertEqual(next_pos, (10, 2))
        self.assertEqual(next_dir, LEFT)


    def test_find_start(self):
        layer = self.load_layer(0)
        detector = Map_Directions_Detector()
        start, direction = detector.find_start(layer)
        self.assertEqual(start, (11, 0))
        self.assertEqual(direction, DOWN)

        layer = self.load_layer(2)
        detector = Map_Directions_Detector()
        start, direction = detector.find_start(layer)
        self.assertEqual(start, (0, 8))
        self.assertEqual(direction, RIGHT)