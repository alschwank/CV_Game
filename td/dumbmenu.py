#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pygame
import sys

# **********************************************************************
# Function "dumbmenu" **************************************************
# **********************************************************************
from constants import WIDTH, FPS


def dumbmenu(screen, menu, menu_title=None, menu_text=(), x_pos=100, y_pos=100, font=None,
             size=70, distance=1.4, menu_text_color=(255, 255, 255), cursorcolor=(255, 0, 0), exitAllowed=True):
    """Draws a Menu using pygame.
    
    Parameters are: screen, menu, x_pos, y_pos, font,
                    size, distance, fgcolor, cursor
                    
    PARAMETERS
    ==========
    screen (Surface): The surface created with pygame.display.set_mode()
                    
    menu (List):      A List of every menupoint that should be visible
    
    x_pos (digit):   Start of x_position, in Pixels (Default: 100)
    
    y_pos (digit):   Start of y_position, in Pixels (Default: 100)
    
    size (digit):    Fontsize (Default: 70)
    
    distance (float):Y-Distance after every single menupoint
                     (Default: 1.4)
    
    fgcolor (Tupel): Foreground-Color, means the Font-Color
                     (Default: (255,255,255), means white)
                     
    cursorcolor (Tupel): Cursor-Color, means that ">"-Charakter
                         (Default: (255,0,0), means red)
                         
    exitAllowed (Bool): If True:
                        If User pressed the ESC-Key, the Cursor will
                        move to the last Menupoint. If Cursorposition
                        is already to the last Menupoint, a pressed
                        ESC-Key will return the latest Menupoint. Very
                        useful if the last Menupoint is something like
                        "Quit Game"...
                        If False:
                        A pressed ESC-Key will takes no effect.
                        (Default: True)
                        
    EXAMPLE
    =======
    import pygame
    from dumbmenu import *
    pygame.init()

    # Just a few static variables
    red   = 255,  0,  0
    green =   0,255,  0

    size = width, height = 640,480    
    screen = pygame.display.set_mode(size)
    screen.fill(blue)
    pygame.display.update()

    print dumbmenu(screen, [
                            'Start Game',
                            'Options',
                            'Manual',
                            'Show Highscore',
                            'Quit Game'],
                            320, 250, "Courier", 32, 1.4, green, red)
    
    HOW TO INTERACT
    ===============
    After called dumbmenu(), the User MUST choose an Menupoint. The
    Script will be haltet until the User makes a decision or a event
    called pygame.QUIT() will be raised.
    
    The User kann pressed directly a Key from 1 to 9 to take the choice.
    Another Method is pressing the UP-/DOWN-Key and take the choice with
    RETURN. Every single Menupoint will get a Number, beginning with 1.
    
    The return-value ist the Number of Menupoint decreased by 1. From
    the above Example: If the User will choice "Manual", the return-
    value will be 2.

    If the number of Menupoints is greater than 9, the numeration will
    continue from A to Z... the return-value is still a number,
    continue from 9 to 34...
    
    If a pygame.QUIT()-Event will be raised, the return-value will be
    -1.
    
    ACTUAL LIMITATIONS
    ==================
    It's actually not possible to change the Font itself.
    
    Drawing Menu will be antialiased. If you want to change that, you'll
    have to change the sourcecode directly.

    OTHERS
    ======
    Yes, I know, my english isn't that good (I'm not a naturally
    speaker) and the sourcecode isn't that good too ;) . It's more or
    less a "quick'n dirty"-Solution. My first intention was to make that
    code for me, but I hope it could may useful for other people too...

    Version: 0.40
    Author: Manuel Kammermeier aka Astorek
    License: MIT

    CHANGES:
    ========
    Version 0.35:
    - First Version
    
    Version 0.40:
    - "bgcolor" removed, now the Function saves the Background
    - added "font", which allows to choose a Systemfont
    """

    # Draw the Menupoints
    pygame.font.init()
    if font is None:
        myfont = pygame.font.Font(None, size)
        title_font = pygame.font.Font(None, size + 10)
    else:
        myfont = pygame.font.SysFont(font, size)
        title_font = pygame.font.SysFont(font, size + 10)
    renderWithChars = False

    # menu title
    if menu_title is not None:
        title = title_font.render(menu_title, True, menu_text_color)
        title_rect = title.get_rect()
        title_rect.centerx = WIDTH // 2
        title_rect.centery = 100
        screen.blit(title, title_rect)
        pygame.display.update(title_rect)

    # normal text (not selectable)
    for index, t in enumerate(menu_text):
        text = myfont.render(str(t), True, menu_text_color)
        textrect = text.get_rect()
        textrect = textrect.move(x_pos, (size // distance * (index - 1)) + y_pos)
        screen.blit(text, textrect)
        pygame.display.update(textrect)

    # menu items (selectable)
    for index, t in enumerate(menu):
        if not renderWithChars:
            text = myfont.render(str(index + 1) + ".  " + str(t), True, menu_text_color)
        else:
            text = myfont.render(chr(char) + ".  " + str(t), True, menu_text_color)
            char += 1
        textrect = text.get_rect()
        textrect = textrect.move(x_pos, (size // distance * (len(menu_text) + index)) + y_pos)
        screen.blit(text, textrect)
        pygame.display.update(textrect)
        if index == 9:
            renderWithChars = True
            char = 65

    # Draw the ">", the Cursor
    cursor = myfont.render(">", True, cursorcolor)
    cursorrect = cursor.get_rect()
    cursorrect = cursorrect.move(x_pos - (size // distance), (size // distance * len(menu_text)) + y_pos)

    # The whole While-loop takes care to show the Cursor, move the
    # Cursor and getting the Keys (1-9 and A-Z) to work...
    ArrowPressed = True
    exitMenu = False
    clock = pygame.time.Clock()
    filler = pygame.Surface.copy(screen)
    fillerrect = filler.get_rect()
    cursorpos = 0
    while True:
        clock.tick(FPS)
        if ArrowPressed:
            screen.blit(filler, fillerrect)
            pygame.display.update(cursorrect)
            cursorrect = cursor.get_rect()
            cursorrect = cursorrect.move(x_pos - (size // distance),
                                         (size // distance * (len(menu_text) + cursorpos)) + y_pos)
            screen.blit(cursor, cursorrect)
            pygame.display.update(cursorrect)
            ArrowPressed = False
        if exitMenu:
            break
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return -1
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE and exitAllowed:
                    if cursorpos == len(menu) - 1:
                        exitMenu = True
                    else:
                        cursorpos = len(menu) - 1
                        ArrowPressed = True

                if 49 <= event.key <= 58 and event.key - 49 < len(menu):
                    return event.key - 49

                elif event.key == pygame.K_UP:
                    ArrowPressed = True
                    if cursorpos == 0:
                        cursorpos = len(menu) - 1
                    else:
                        cursorpos -= 1
                elif event.key == pygame.K_DOWN:
                    ArrowPressed = True
                    if cursorpos == len(menu) - 1:
                        cursorpos = 0
                    else:
                        cursorpos += 1
                elif event.key == pygame.K_KP_ENTER or event.key == pygame.K_RETURN:
                    exitMenu = True
    
    return cursorpos

if __name__ == '__main__':
    sys.stderr.write("You should import me, not start me...")
    sys.exit()
