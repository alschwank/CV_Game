from unittest import TestCase

import numpy as np

from vector_match import Line


__author__ = 'Alexander'


class TestLine(TestCase):
    def test_init(self):
        np_array = np.array((1, 2, 3, 4))
        line = Line(np_array=np_array)
        self.assertEqual(1, line.start.x)
        self.assertEqual(2, line.start.y)
        self.assertEqual(3, line.end.x)
        self.assertEqual(4, line.end.y)

    def test_calc_slope(self):
        np_array = np.array((1, 1, 2, 2))
        line = Line(np_array=np_array)
        self.assertEqual(1, line.slope)

        np_array = np.array((1, 1, 2, 1.5))
        line = Line(np_array=np_array)
        self.assertEqual(0.5, line.slope)

        np_array = np.array((1, 1, 2, 3))
        line = Line(np_array=np_array)
        self.assertEqual(2, line.slope)

    def test_merge(self):
        np_array = np.array((0, 0, 1, 1))
        line0 = Line(np_array=np_array)

        np_array = np.array((3, 3, 4, 4))
        line1 = Line(np_array=np_array)

        line0.merge(line1)
        self.assertEqual(0, line0.start.x)
        self.assertEqual(0, line0.start.y)
        self.assertEqual(4, line0.end.x)
        self.assertEqual(4, line0.end.y)


    def test_is_similar(self):
        #0 - TRUE
        np_array = np.array((0, 0, 1, 1))
        line00 = Line(np_array=np_array)

        np_array = np.array((1, 1, 2, 2))
        line01 = Line(np_array=np_array)
        self.assertTrue(line00.is_similar(line01))

        #1 - TRUE
        np_array = np.array((0, 0, 1, 1))
        line10 = Line(np_array=np_array)

        np_array = np.array((2, 2, 4, 4))
        line11 = Line(np_array=np_array)
        self.assertTrue(line10.is_similar(line11))

        #2 - FALSE
        np_array = np.array((0, 0, 1, 1))
        line20 = Line(np_array=np_array)

        np_array = np.array((1, 1, 2, 2.3))
        line21 = Line(np_array=np_array)
        self.assertFalse(line20.is_similar(line21))

        #3 - FALSE
        np_array = np.array((0, 0, 1, 1))
        line30 = Line(np_array=np_array)

        np_array = np.array((10, 10, 11, 11))
        line31 = Line(np_array=np_array)
        self.assertFalse(line30.is_similar(line31))